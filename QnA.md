# Questions and Answers
Please spend some time to answer the following questions. It will help us to understand you and your thought process more. You may provide your responses in your email.

1. [How many hours did you spend on the test?](#how-many-hours-did-you-spend-on-the-test)
2. [If you are given 7 more days, what further improvements or enhancements would you implement?](#if-you-are-given-7-more-days-what-further-improvements-or-enhancements-would-you-implement)
3. [Which parts of your work are you most proud of? Please explain.](#which-parts-of-your-work-are-you-most-proud-of-please-explain)
4. [Which parts of the challenge do you spend the most time on? What did you find most difficult? Please explain.](#which-parts-of-the-challenge-do-you-spend-the-most-time-on-what-did-you-find-most-difficult-please-explain)
5. [Did you use any libraries for the challenge? What are they used for? Why did you choose these libraries and not the alternatives?](#did-you-use-any-libraries-for-the-challenge-what-are-they-used-for-why-did-you-choose-these-libraries-and-not-the-alternatives)
6. [What do you think of the challenge overall?](#what-do-you-think-of-the-challenge-overall)
7. [How we can improve the challenge? We would love to hear them.](#how-we-can-improve-the-challenge-we-would-love-to-hear-them)
8. [Any other thoughts that you wish to share with us?](#any-other-thoughts-that-you-wish-to-share-with-us)

---

### How many hours did you spend on the test?
I started at 7 PM on 11 Nov and will be finishing at 3 AM on 16 Nov. It should be around 5 days and more than 8 hours per day to work with this assignment.

### If you are given 7 more days, what further improvements or enhancements would you implement?
Have a lot of things can apply, but only for 7 days so I decided like below.

**For the features**

1. Auth features: Login / Logout / Forgot password / expired Token and Refresh Token
2. History: Should have to display the history at the home page.
3. Use Stripe for top-up flow: after submit the form to do top up, I will display Stripe to make the fake flow to up from credit card. The flow will have more background step jobs to make sure the Transaction is consistency.

**For the code structure and system**

1. Apply TypeScript for Frontend: I used TypeScript before, TypeScript can help to handle a lot worst-case and reduce test cases for unit test, reduce time to pair coding or teamwork. Make code clean and have a good structure, good for maintenance.
2. Microservices: the flow to check the exchange rate can make it become an independent service, make top-up flow become a service call is payment

### Which parts of your work are you most proud of? Please explain.

**UI/UX**

- For the few years to work at the Front-End position, I also spent a lot of time to learn about the UI/UX and the color meaning. Applied this to this project I choose the primary color is the violet - the luxury, trustable and straightforward - with the design is a bit of flat design to bring the feeling about modernness and fast.

**Database**

- I apply to use the `Procedures` much as I can on the database, and any action will cause change on the database, it should go the thought of the procedures. Procedures help to protect the data and only give enough data to the backend layer so it can help to reduce the connection open from the backend.

- And another reason, I saw a lot of Engineers they are using ORM libs to query data without the optimize. Sometimes, they send a query to get a million records to load into RAM and do the filter on the Backend side, to get the result of five hundred. Those things can do on the database.

### Which parts of the challenge do you spend the most time on? What did you find most difficult? Please explain.

The parts I proud - are also the parts I spent a lot of time.

**UI/UX**

- For example, for the exchange flow, I tried to optimize the flow to best for UX, I could design it for the more simple. And it's also caused a lot of edge case I need to do handle then It also takes my time.

**Database**

- Very long time didn't touch to backend and database, so I need things a lot to design the database can make sure the privacy, I decided to hide the real balance of any wallet. Hence DB Administrators can't view the balance of the customer.

- If anyone tries to view the balance, they need to call the query to decrypt the balance, and the logs system stalks them.

### Did you use any libraries for the challenge? What are they used for? Why did you choose these libraries and not the alternatives?

**Frontend**

- I use the ReactJS framework because it's familiar with my tech stack, for another library it's `lodash`. Lodash helps me a lot for the check the data and get data.

**Backend**

- I use Echo Framework, and it's the minimalist framework for Golang, enough for the assignment. But if I need to deploy the microservices, so I suggest we shouldn't use any web framework, should use RPC to communicate between services.

### What do you think of the challenge overall?
### How we can improve the challenge? We would love to hear them.

The assignment is good, enough to test the candidates. But have a few things that need to improve:
- The request/response parameter should follow one syntax because I saw sometime first char is capitalized and some time is not.

- Should reduce the scope of the assignment. I spent a lot of time per day to solve tasks, but it is not enough time to do. The frontend has a lot of small things need to solve to good for UI, it's unlike the backend.


### Any other thoughts that you wish to share with us?

Haven't anything to share with you, I need to research and understand more about the biz side so maybe I will have a lot question for you.

One thing I want to share, because last time you said the product will deploy in Cambodia and this country, they are still using cash for the original payment way. So you can research `lumitel` the e-wallet for Burundi market, they spent 2 years to take 60% of the market and they have revenue from this year (that is the craziest thing, a lot of big companies keep burning money to take the market and without the revenue), just got the Award about the fastest growth this year.

I came to Cambodia before, I saw the market in there are the same with Burundi, I think you can learn a lot from this product.

---

Thank you for your time! :)