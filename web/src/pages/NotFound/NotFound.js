import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import Layout from 'components/Layout';
import styles from './NotFound.module.scss';

export default class NotFound extends PureComponent {
  componentWillMount() {
    document.title = '404: The page you are looking for does not exist';
  }

  render = () => (
    <Layout showBackground>
      <Container className={styles.container}>
        <Row>
          <Col xs={12} md={10} lg={8} className="m-auto">
            <h1 className={styles.code404}>404</h1>
            <h4 className={styles.message}>The page you are looking for does not exist.</h4>
            <p className={styles.backToHome}>
              Please <Link to="/">back to home</Link>.
            </p>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
}
