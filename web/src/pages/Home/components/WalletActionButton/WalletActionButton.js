import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'common/helpers/classnames';
import styles from './WalletActionButton.module.scss';

export default class WalletActionButton extends PureComponent {
  static propTypes = {
    className:        PropTypes.string,
    icon:             PropTypes.string.isRequired,
    text:             PropTypes.string,
    formattedMessage: PropTypes.object,
    // 
    onClick:          PropTypes.func,
  }

  static defaultProps = {
    className:        '',
    text:             '',
    formattedMessage: null,
    icon:             '',
    // 
    onClick:          () => {},
  }

  render() {
    const { icon, text, formattedMessage, onClick, className } = this.props;
    return (
      <button
        className={classnames([styles.wrapper, className])}
        onClick={onClick}
      >
        <span className={styles.container}>
          <span className={styles.icon}>
            <span className={icon} />
          </span>
          <span className={styles.text}>
            {text || formattedMessage || ''}
          </span>
        </span>
      </button>
    )
  }
}
