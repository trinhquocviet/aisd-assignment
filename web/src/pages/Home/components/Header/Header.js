import React, { Component } from 'react';
import WalletActionButton from '../WalletActionButton';
import ModalTopUp from '../ModalTopUp';
import ModalExchange from '../ModalExchange';
import styles from './Header.module.scss';
import { FormattedMessage } from 'react-intl';

export default class Header extends Component {

  state = {
    showModalTopUp: false,
    showModalSendMoney: false,
  }

  handleUpdateModal = (key, newValue) => {
    this.setState({ [key]: newValue });
  }

  render() {
    const { showModalTopUp, showModalSendMoney } = this.state;

    return (
      <div className={styles.wrapper}>
        <WalletActionButton
          className={styles.button}
          icon="fas fa-money-check-alt"
          formattedMessage={<FormattedMessage id="top up" defaultMessage="Top Up" />}
          onClick={() => this.handleUpdateModal('showModalTopUp', !showModalTopUp)}
        />

        <WalletActionButton
          className={styles.button}
          icon="fas fa-exchange-alt"
          formattedMessage={<FormattedMessage id="exchange" defaultMessage="EXCHANGE" />}
          onClick={() => this.handleUpdateModal('showModalSendMoney', !showModalSendMoney)}
        />

        <ModalTopUp
          isOpen={showModalTopUp}
          toggle={() => this.setState({ showModalTopUp: !showModalTopUp })}
        />

        <ModalExchange
          isOpen={showModalSendMoney}
          toggle={() => this.setState({ showModalSendMoney: !showModalSendMoney })}
        />
      </div>
    )
  }
}
