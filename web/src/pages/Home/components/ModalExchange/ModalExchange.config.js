import React from 'react';
import localeActions from 'actions/locale';
import FormSG from './Form.SG';
import FormUS from './Form.US';
import FormMY from './Form.MY';
import { pick, get } from 'lodash';

const FORM = {
  'sg': FormSG,
  'us': FormUS,
  'my': FormMY,
}

export const DynamicForm = (props) => {
  const locale = localeActions.get();
  const Form = get(FORM, locale, FormSG);
  return (<Form {...pick(props, ['styles'])} />);
}