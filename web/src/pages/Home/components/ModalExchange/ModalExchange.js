import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ModalBox from 'components/ModalBox';
import { FormattedMessage } from 'react-intl';
import { DynamicForm } from './ModalExchange.config';
import styles from './ModalExchange.module.scss';

export default class ModalExchange extends Component {
  static propTypes = {
    isOpen:       PropTypes.bool,
    toggle:       PropTypes.func,
  };
  
  static defaultProps = {
    isOpen:       false,
    toggle:       () => {},
  };

  render() {
    return (
      <ModalBox
        size="sm"
        formattedMessage={ <FormattedMessage id="exchange" defaultMessage="EXCHANGE" /> }
        isOpen={this.props.isOpen}
        toggle={this.props.toggle}
        className={styles.wrapper}
      >
        <DynamicForm styles={styles} />
      </ModalBox>
    )
  }
}
