import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import moment from 'moment';
import classnames from 'classnames';
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  InputGroup,
  Progress,
} from 'reactstrap';
import numeral from 'numeral';
import { FormattedMessage } from 'react-intl';
import { isEqual, isEmpty, pick } from 'lodash';
import { ROUTES } from 'common/constants/routes';
import exchangeActions from 'actions/exchange';
import { NUMERAL_FORMAT } from 'common/constants/env';
import walletActions from 'actions/wallet';
import CurrencyInputGroupButtonDropdown from 'components/CurrencyInputGroupButtonDropdown';

// const DEFAULT_ERROR = "Can't get or calculate the exchange rate, please try again. (Note: from-to currencies should be a difference)";

class FormUS extends Component {
  static propTypes = {
    history:            PropTypes.object,
    styles:             PropTypes.object,
    // 
    currencies:         PropTypes.array,
    rate:               PropTypes.string,
    amount:             PropTypes.string,
    requestTime:        PropTypes.number,
    isCalculatingRate:  PropTypes.bool,
    calculateRateError: PropTypes.string,
    transactions:       PropTypes.array,
    isSendingMoney:     PropTypes.bool,
    sendMoneyError:     PropTypes.string,
    // 
    doCalculateRate:    PropTypes.func,
    doSendMoney:        PropTypes.func,
  }

  static propTypes = {
    styles:             {},
    // 
    currencies:         [],
    rate:               '0',
    amount:             '0',
    requestTime:        0,
    isCalculatingRate:  false,
    calculateRateError: '',
    transactions:       [],
    isSendingMoney:     false,
    sendMoneyError:     '',
    // 
    doCalculateRate:    () => {},
    doSendMoney:        () => {},
  }

  state = {
    fromCurrencyID:     0,
    toCurrencyID:       0,
    type:               'FROM',
    fromAmount:         '',
    toAmount:           '',
    changedAt:          null,
  }

  componentDidUpdate(prevProps, prevState) {
    this.redirectToNextStep(prevProps);
  }

  redirectToNextStep = (prevProps) => {
    const [prevIsSendingMoney, curIsSendingMoney] = [prevProps.isSendingMoney, this.props.isSendingMoney];
    if (prevIsSendingMoney && curIsSendingMoney !== prevIsSendingMoney) {
      const [error, transactions] = [this.props.sendMoneyError, this.props.transactions];
      const { history } = this.props;
      if (error.length > 0) {
        history.push(ROUTES.EXCHANGE_ERROR.link, { error });
      } else {
        history.push(ROUTES.EXCHANGE_SUCCESS.link, { transactions });
      }
    }
  }

  getCurrencyCodeByID = (currencyID) => {
    const { currencies } = this.props;
    const currency = (currencies || []).find(c => c.currencyID === currencyID);
    return !isEmpty(currency) ? currency.code : 'NaN';
  }

  isAllowCalculateRate = () => {
    const { fromCurrencyID, toCurrencyID, fromAmount, toAmount, type } = this.state;
    const fromAmountNumber = numeral(fromAmount || '0');
    const toAmountNumber = numeral(toAmount || '0');

    return (fromCurrencyID !== toCurrencyID
      && ( (type === 'FROM' && fromAmountNumber.difference(0) !== 0)
          || (type === 'TO' && toAmountNumber.difference(0) !== 0)
      )
    );
  }

  isAllowSubmit = () => {
    const { amount, rate } = this.props;

    return (this.isAllowCalculateRate()
      && numeral(amount || '0').difference('0') !== 0
      && numeral(rate || '0').difference('0') !== 0
    );
  }

  handleOnChange = (key, value) => {
    const newState = { [key]: value, changedAt: moment().unix() };
    this.setState(newState);
  }

  handleChangeType = (newType) => {
    const { type } = this.state;
    if (newType !== type) {
      this.setState({
        type:       newType,
        changedAt:  moment().unix()
      });
    }
  }

  handleOnCalculateRate = () => {
    const { doCalculateRate } = this.props;
    if (doCalculateRate) {
      const { fromCurrencyID, toCurrencyID, type, fromAmount, toAmount } = this.state;
      const data = {
        type: "FROM",
        fromCurrencyID: (type === "FROM") ? fromCurrencyID : toCurrencyID,
        toCurrencyID: (type === "FROM") ? toCurrencyID : fromCurrencyID,
        amount: (type === "FROM") ? fromAmount : toAmount,
      };
      doCalculateRate(data);
    }
  }

  handleOnSubmit = (e) => {
    e.preventDefault();
    const { doSendMoney, amount, rate } = this.props;

    if (doSendMoney) {
      const { fromCurrencyID, toCurrencyID, type, toAmount, changedAt } = this.state;
      const reqData = {
        fromCurrencyID,
        toCurrencyID,
        type: "FROM",
        amount: (type === 'FROM') ? amount : numeral(toAmount || '0')
                                              .divide(rate || '1')
                                              .format(NUMERAL_FORMAT, Math.ceil)
      };

      doSendMoney(reqData, changedAt);
    }
  }

  renderExchangeRateText = () => {
    const { isCalculatingRate, calculateRateError, amount, rate, styles } = this.props;

    if (!isCalculatingRate && !isEmpty(calculateRateError)) {
      return (
        <span className="text-danger">
          {<FormattedMessage id="exchange_form.exchange_rate_text_default_error" defaultMessage="error" />}
        </span>
      );
    }

    if (isCalculatingRate || isEqual(rate, '0') || isEqual(amount, '0')) {
      return (<Progress animated className={styles.loadingRate} color="gray-400" value="100" />);
    }

    const { fromCurrencyID, toCurrencyID, fromAmount, toAmount, type } = this.state;
    const fromCurrencyCode = this.getCurrencyCodeByID(type === 'FROM' ? fromCurrencyID : toCurrencyID);
    const toCurrencyCode = this.getCurrencyCodeByID(type === 'FROM' ? toCurrencyID : fromCurrencyID);
    let needToSend = fromAmount;
    if (type === 'TO') {
      needToSend = numeral(toAmount || '0')
                    .divide(rate || '1')
                    .format(NUMERAL_FORMAT, Math.ceil);
    }
    return (
      <Fragment>
        <p className="text-center mb-0">
          <FormattedMessage
            id="exchange_form.exchange_rate_text"
            defaultMessage={`<b>1</b> {fromCurrencyCode} equal <i>{rate}</i> {toCurrencyCode}`}
            values={{
              b: msg => (<b>{msg}</b>),
              i: () => (<b><span className="text-success">{rate}</span></b>),
              rate,
              fromCurrencyCode,
              toCurrencyCode,
            }}
          />
        </p>
        <p className="text-center mb-0">
          <FormattedMessage
            id="exchange_form.send_total_amount"
            defaultMessage="Send total {amount} {currencyCode}"
            values={{
              amount: needToSend,
              currencyCode: fromCurrencyCode,
            }}
          />
        </p>
      </Fragment>
    );
  }

  render() {
    const { styles } = this.props;
    const {
      fromCurrencyID,
      toCurrencyID,
      fromAmount,
      toAmount,
      type,
    } = this.state;

    const formGroupClasses = (type === 'FROM') ? 'border-success' : styles.blur;
    const toGroupClasses = (type === 'TO') ? 'border-success' : styles.blur;
    
    return (
      <Form
        onSubmit={(e) => this.handleOnSubmit(e)}
      >
        <div
          className={classnames(["d-block border p-2 rounded mb-3", formGroupClasses])}
        >
          <FormGroup className="mb-0">
            <Label for="from-amount">
              <FormattedMessage
                id="exchange_form.i_want_to_exchange_from_to"
                defaultMessage="I want to exchange <b>{fromCurrencyCode}</b> to <b>{toCurrencyCode}</b>"
                values={{
                  b: msg => (<b>{msg}</b>),
                  fromCurrencyCode: this.getCurrencyCodeByID(fromCurrencyID),
                  toCurrencyCode: this.getCurrencyCodeByID(toCurrencyID),
                }}
              />
            </Label>
            <InputGroup>
              <CurrencyInputGroupButtonDropdown
                addonType="prepend"
                displayType="flag"
                selectedCurrencyCode={this.getCurrencyCodeByID(fromCurrencyID)}
                onChange={(v) => { this.handleOnChange('fromCurrencyID', v) }}
              />
              <Input
                type="number" name="from-amount" id="from-amount" placeholder="Amount" min="0" max="10000" step="0.01" autoComplete="false"
                required={!!(type === 'FROM')}
                value={fromAmount}
                onChange={(e) => this.handleOnChange('fromAmount', e.target.value)}
                onFocus={() => this.handleChangeType('FROM')}
              />
              <CurrencyInputGroupButtonDropdown
                addonType="append"
                displayType="flag"
                selectedCurrencyCode={this.getCurrencyCodeByID(toCurrencyID)}
                onChange={(v) => { this.handleOnChange('toCurrencyID', v) }}
              />
            </InputGroup>
          </FormGroup>
        </div>

        <div
          className={classnames(["d-block border p-2 rounded mb-3", toGroupClasses])}
        >
          <FormGroup className="mb-0">
            <Label for="to-amount">
              <FormattedMessage
                id="exchange_form.i_want_to_receive_from_to"
                defaultMessage="I want to receive <b>{fromCurrencyCode}</b> from <b>{toCurrencyCode}</b>"
                values={{
                  b: msg => (<b>{msg}</b>),
                  fromCurrencyCode: this.getCurrencyCodeByID(fromCurrencyID),
                  toCurrencyCode: this.getCurrencyCodeByID(toCurrencyID),
                }}
              />
            </Label>
              <InputGroup>
                <CurrencyInputGroupButtonDropdown
                  addonType="prepend"
                  displayType="flag"
                  selectedCurrencyCode={this.getCurrencyCodeByID(fromCurrencyID)}
                  onChange={(v) => { this.handleOnChange('fromCurrencyID', v) }}
                />
                <Input
                  type="number" name="to-amount" id="to-amount" placeholder="Amount" min="0" max="10000" step="0.01" autoComplete="false"
                  required={!!(type === 'TO')}
                  value={toAmount}
                  onChange={(e) => this.handleOnChange('toAmount', e.target.value)}
                  onFocus={() => this.handleChangeType('TO')}
                />
                <CurrencyInputGroupButtonDropdown
                  addonType="append"
                  displayType="flag"
                  selectedCurrencyCode={this.getCurrencyCodeByID(toCurrencyID)}
                  onChange={(v) => { this.handleOnChange('toCurrencyID', v) }}
                />
              </InputGroup>
            </FormGroup>
        </div>

        <Button
          type="button"
          color="blue-violet"
          className="rounded-pill w-100"
          disabled={!this.isAllowCalculateRate()}
          onClick={() => this.handleOnCalculateRate()}
        >
          <FormattedMessage id="exchange_form.Calculate Rate" defaultMessage="Calculate Rate" />
        </Button>
        
        <div className={classnames(['d-block fade in', this.isAllowCalculateRate() && 'show'])}>
          <hr/>
          <h6 className="text-center">
            <FormattedMessage id="exchange_form.Exchange Info" defaultMessage="Exchange Info" />
          </h6>
          <p className="mb-0 text-center">
            { this.renderExchangeRateText() }
          </p>
          <hr/>
        </div>
        
        <Button
          type="submit"
          color="blue-violet"
          className={classnames(["rounded-pill w-100 fade in", this.isAllowCalculateRate() && 'show'])}
          disabled={!this.isAllowSubmit()}
        >
          <FormattedMessage id="exchange_form.Exchange Now" defaultMessage="Exchange Now" />
        </Button>
      </Form>
    )
  }
}

const mapStateToProps = state => ({
  ...pick(state.currencyReducer, [
    'currencies',
  ]),

  ...pick(state.exchangeReducer, [
    'rate',
    'amount',
    'requestTime',
    'isCalculatingRate',
    'calculateRateError',
  ]),
  ...pick(state.walletReducer, [
    'transactions',
    'isSendingMoney',
    'sendMoneyError',
  ])
});

const mapDispatchToProps = dispatch => ({
  doCalculateRate: (data) => dispatch(exchangeActions.calculateRate(data)),
  doSendMoney: (data) => dispatch(walletActions.sendMoney(data)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FormUS));