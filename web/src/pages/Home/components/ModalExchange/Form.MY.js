import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  InputGroup,
  Progress,
} from 'reactstrap';
import numeral from 'numeral';
import { FormattedMessage } from 'react-intl';
import { isEqual, isEmpty, pick } from 'lodash';
import { ROUTES } from 'common/constants/routes';
import { NUMERAL_FORMAT } from 'common/constants/env';
import exchangeActions from 'actions/exchange';
import walletActions from 'actions/wallet';
import CurrencyInputGroupButtonDropdown from 'components/CurrencyInputGroupButtonDropdown';

// const DEFAULT_ERROR = "Can't get or calculate the exchange rate, please try again. (Note: from currencies should be a difference)";

class FormMY extends Component {
  static propTypes = {
    history:        PropTypes.object,
    styles:         PropTypes.object,
    // 
    currencies:     PropTypes.array,
    rate:           PropTypes.string,
    requestTime:    PropTypes.number,
    isGettingRate:  PropTypes.bool,
    getRateError:   PropTypes.string,
    transactions:   PropTypes.array,
    isSendingMoney: PropTypes.bool,
    sendMoneyError: PropTypes.string,
    // 
    doGetRate:      PropTypes.func,
    doSendMoney:    PropTypes.func,
  }

  static propTypes = {
    styles:         {},
    // 
    currencies:     [],
    rate:           '0',
    requestTime:    0,
    isGettingRate:  false,
    getRateError:   '',
    transactions:   [],
    isSendingMoney: false,
    sendMoneyError: '',
    // 
    doGetRate:      () => {},
    doSendMoney:    () => {},
  }

  state = {
    fromCurrencyID: 0,
    toCurrencyID:   0,
    fromAmount:     '',
    toAmount:       '',
    rate:           '0',
    changedAt:      null,
  }

  static getDerivedStateFromProps(props, state) {
    let newState = {};

    const [newRate, curRate] = [props.rate, state.rate];
    if (!isEqual(newRate, curRate)) {
      const { fromAmount } = state;
      newState.toAmount = numeral(fromAmount || '0').multiply(newRate).format(NUMERAL_FORMAT);
      newState.rate = newRate;
    }

    return (isEmpty(newState)) ? null : newState;
  }

  componentDidUpdate(prevProps, prevState) {
    this.updateRate(prevState);
    this.redirectToNextStep(prevProps);
  }

  updateRate = (prevState) => {
    const [prevFromCurrencyID, prevToCurrencyID] = [prevState.fromCurrencyID, prevState.toCurrencyID];
    const [curFromCurrencyID, curToCurrencyID] = [this.state.fromCurrencyID, this.state.toCurrencyID];
    const [prevType, curType] = [prevState.type, this.state.type];
    if (!isEqual(prevFromCurrencyID, curFromCurrencyID)
      ||!isEqual(prevToCurrencyID, curToCurrencyID)
      ||!isEqual(prevType, curType)
    ) {
      const { doGetRate } = this.props;
      const { changedAt } = this.state;
      if (doGetRate) {
        return doGetRate({
          fromCurrencyID: curFromCurrencyID,
          toCurrencyID: curToCurrencyID,
        }, changedAt);
      }
    }

    return null;
  }

  redirectToNextStep = (prevProps) => {
    const [prevIsSendingMoney, curIsSendingMoney] = [prevProps.isSendingMoney, this.props.isSendingMoney];
    if (prevIsSendingMoney && curIsSendingMoney !== prevIsSendingMoney) {
      const [error, transactions] = [this.props.sendMoneyError, this.props.transactions];
      const { history } = this.props;
      if (error.length > 0) {
        history.push(ROUTES.EXCHANGE_ERROR.link, { error });
      } else {
        history.push(ROUTES.EXCHANGE_SUCCESS.link, { transactions });
      }
    }
  }

  getCurrencyCodeByID = (currencyID) => {
    const { currencies } = this.props;
    if (currencies.length > 0) {
      const currency = currencies.find(c => c.currencyID === currencyID);
      if (!isEmpty(currency)) {
        return currency.code;
      }
    }

    return 'NaN';
  }

  isAllowSubmit = () => {
    const { fromCurrencyID, toCurrencyID, fromAmount, toAmount, rate } = this.state;
    const fromAmountNumber = numeral(fromAmount || '0');
    const toAmountNumber = numeral(toAmount || '0');

    return (fromCurrencyID !== toCurrencyID
      && fromAmountNumber.difference(0) !== 0 && toAmountNumber.difference(0) !== 0
      && (fromAmountNumber.multiply(rate).difference(toAmount || '0') === 0
          || toAmountNumber.multiply(rate).difference(fromAmount || '0') === 0)
    );
  }

  handleOnChange = (key, value) => {
    const newState = { [key]: value, changedAt: moment().unix() };

    if (key === 'fromAmount') {
      const { rate } = this.state;
      newState.toAmount = numeral(value || '0').multiply(rate).format(NUMERAL_FORMAT);
    }
    
    this.setState(newState);
  }

  handleOnSubmit = (e) => {
    e.preventDefault();
    const { doSendMoney } = this.props;

    if (doSendMoney) {
      const { fromCurrencyID, toCurrencyID, fromAmount } = this.state;
      const reqData = {
        fromCurrencyID,
        toCurrencyID,
        type: "FROM",
        amount: fromAmount,
      };

      doSendMoney(reqData);
    }
  }

  renderExchangeRateText = () => {
    const { isGettingRate, getRateError, styles } = this.props;

    if (isGettingRate) {
      return (<Progress animated className={styles.loadingRate} color="gray-400" value="100" />);
    }
    
    if (!isGettingRate && !isEmpty(getRateError)) {
      return (
        <span className="text-danger">
          {<FormattedMessage id="exchange_form.exchange_rate_text_default_error" defaultMessage="error" />}
        </span>
      );
    }

    const { fromCurrencyID, toCurrencyID, rate } = this.state;
    const fromCurrencyCode = this.getCurrencyCodeByID(fromCurrencyID);
    const toCurrencyCode = this.getCurrencyCodeByID(toCurrencyID);

    return (
      <FormattedMessage
        id="exchange_form.exchange_rate_text"
        defaultMessage={`<b>1</b> {fromCurrencyCode} equal <i>{rate}</i> {toCurrencyCode}`}
        values={{
          b: msg => (<b>{msg}</b>),
          i: () => (<b><span className="text-success">{rate}</span></b>),
          rate,
          fromCurrencyCode,
          toCurrencyCode,
        }}
      />
    );
  }

  render() {
    const {
      fromAmount,
      toAmount,
    } = this.state;
    
    return (
      <Form
          onSubmit={(e) => this.handleOnSubmit(e)}
        >
        <FormGroup>
          <Label for="from-amount">
            <FormattedMessage id="exchange_form.I want to exchange" defaultMessage="I want to exchange" />
          </Label>
          <InputGroup>
            <CurrencyInputGroupButtonDropdown
              addonType="prepend"
              displayType="flag"
              onChange={(v) => { this.handleOnChange('fromCurrencyID', v) }}
            />
            <Input
              type="number" name="from-amount" id="from-amount" placeholder="Amount" min="0" max="10000" step="0.01" required autoComplete="false"
              value={fromAmount}
              onChange={(e) => this.handleOnChange('fromAmount', e.target.value)}
              // onFocus={() => this.handleOnFocus('fromAmount')}
            />
          </InputGroup>
        </FormGroup>

        <FormGroup>
          <Label for="to-amount">
            <FormattedMessage id="exchange_form.To" defaultMessage="To" />
          </Label>
          <InputGroup>
            <CurrencyInputGroupButtonDropdown
              addonType="prepend"
              displayType="flag"
              onChange={(v) => { this.handleOnChange('toCurrencyID', v) }}
            />
            <Input
              type="number" name="to-amount" id="to-amount" placeholder="Amount" required disabled
              value={toAmount}
            />
          </InputGroup>
        </FormGroup>
        
        <div className="d-block">
          <hr/>
          <h6 className="text-center">
            <FormattedMessage id="exchange_form.Exchange Rate" defaultMessage="Exchange Rate" />
          </h6>
          <p className="mb-0 text-center">
            { this.renderExchangeRateText() }
          </p>
          <hr/>
        </div>
        
        <Button
          type="submit"
          color="blue-violet"
          className="rounded-pill w-100"
          disabled={!this.isAllowSubmit()}
        >
          <FormattedMessage id="exchange_form.Exchange Now" defaultMessage="Exchange Now" />
        </Button>
      </Form>
    )
  }
}

const mapStateToProps = state => ({
  ...pick(state.currencyReducer, [
    'currencies',
  ]),

  ...pick(state.exchangeReducer, [
    'rate',
    'requestTime',
    'isGettingRate',
    'getRateError',
  ]),
  ...pick(state.walletReducer, [
    'transactions',
    'isSendingMoney',
    'sendMoneyError',
  ])
});

const mapDispatchToProps = dispatch => ({
  doGetRate: (data) => dispatch(exchangeActions.getRate(data)),
  doSendMoney: (data) => dispatch(walletActions.sendMoney(data)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FormMY));