import React, { Component } from 'react';
import { connect } from 'react-redux';
import numeral from 'numeral';
import {
  defaultProps,
  propTypes,
  mapStateToProps,
} from './WalletList.redux';
import { COUNTRIES } from 'common/constants/countries';
import styles from './WalletList.module.scss';

class WalletList extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  renderWallet = (wallet) => {
    const { currencies } = this.props;
    const {
      Amount: amount,
      CurrencyID: currencyID,
      WalletID: walletID,
    } = wallet;
    const { code: code = '' } = currencies.find(c => c.currencyID === currencyID);
    const { currency_symbol: symbol = '' } = COUNTRIES.find(c => c.currency_code === code);

    return (
      <div key={`wallet-${walletID}`} className={styles.wallet}>
        <div className={styles.walletContent}>
          <h5 className={styles.walletCode}>{ code }</h5>
          <p className={styles.walletBalance}>{ numeral(amount).format('0,0[.]00') }</p>
          <span className={styles.walletSymbol}>{ symbol }</span>
        </div>
      </div>
    );
  }

  render() {
    const { wallets, currencies } = this.props;

    return (
      <div className={styles.wrapper}>
        <div className={styles.list}>
          { currencies.length > 0 && wallets.map(this.renderWallet) }
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, null)(WalletList);
