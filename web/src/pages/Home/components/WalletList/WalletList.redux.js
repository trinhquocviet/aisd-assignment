import PropTypes from 'prop-types';
import { pick } from 'lodash';
// import userActions from 'actions/user';
// import currencyActions from 'actions/currency';

const propTypes = {
  // 
  // profile:          PropTypes.object,
  wallets:          PropTypes.array,
  currencies:       PropTypes.array,
  // isGettingProfile: PropTypes.bool,
  // isGettingWallets: PropTypes.bool,
  // getProfileError:  PropTypes.string,
  // getWalletsError:  PropTypes.string,
  //
  // doGetUserProfile: PropTypes.func,
  // doGetUserWallets: PropTypes.func,
  // doGetCurrencies:  PropTypes.func,
};

const defaultProps = {
  // profile:          {},
  wallets:          [],
  currencies:       [],
  // isGettingProfile: false,
  // isGettingWallets: false,
  // getProfileError:  '',
  // getWalletsError:  '',
  // 
  // doGetUserProfile: () => {},
  // doGetUserWallets: () => {},
  // doGetCurrencies:  () => {}
};

const mapStateToProps = state => ({
  ...pick(state.userReducer, [
    // 'profile',
    'wallets',
    // 'isGettingProfile',
    // 'isGettingWallets',
    // 'getProfileError',
    // 'getWalletsError'
  ]),
  ...pick(state.currencyReducer, [
    'currencies',
  ]),
});

// const mapDispatchToProps = dispatch => ({
//   doGetUserProfile: () => dispatch(userActions.getProfile()),
//   doGetUserWallets: () => dispatch(userActions.getWallets()),
//   doGetCurrencies: () => dispatch(currencyActions.getLookUpValues()),
// });

export {
  defaultProps,
  propTypes,
  mapStateToProps,
  // mapDispatchToProps,
};