import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  InputGroup,
} from 'reactstrap';
import ModalBox from 'components/ModalBox';
import CurrencyInputGroupButtonDropdown from 'components/CurrencyInputGroupButtonDropdown';
import { ROUTES } from 'common/constants/routes';
import { FormattedMessage } from 'react-intl';
import {
  defaultProps,
  propTypes,
  mapStateToProps,
  mapDispatchToProps,
} from './ModalTopUp.redux';

class ModalTopUp extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  state = {
    currencyID:   0,
    amount:       0,
    description:  '',
  }

  componentDidUpdate(prevProps) {
    const [prevIsTopUp, curIsTopUp] = [prevProps.isTopUp, this.props.isTopUp];
    if (prevIsTopUp && curIsTopUp !== prevIsTopUp) {
      const [error, transaction] = [this.props.topUpError, this.props.transaction];
      const { history } = this.props;
      if (error.length > 0) {
        history.push(ROUTES.TOPUP_ERROR.link, { error });
      } else {
        history.push(ROUTES.TOPUP_SUCCESS.link, { transaction });
      }
    }
  }

  handleOnSubmit = (e) => {
    e.preventDefault();
    const { doTopUp } = this.props;

    if (doTopUp) {
      const { currencyID, amount, description } = this.state;
      const reqData = {
        currencyID,
        amount,
        description,
        receipt: "",
      };

      doTopUp(reqData);
    }
  }

  render() {
    return (
      <ModalBox
        size="sm"
        formattedMessage={ <FormattedMessage id="top up" defaultMessage="TOP UP" /> }
        isOpen={this.props.isOpen}
        toggle={this.props.toggle}
      >
        <Form
          onSubmit={(e) => this.handleOnSubmit(e)}
        >
          <FormGroup>
            <Label for="amount">
              { <FormattedMessage id="top_up_form.Currency / Amount" defaultMessage="Currency / Amount" /> } <span className="text-danger">*</span>
            </Label>
            <InputGroup>
              <CurrencyInputGroupButtonDropdown
                addonType="prepend"
                displayType="flag"
                onChange={(v) => { this.setState({ currencyID: v }) }}
              />
              <Input onChange={(e) => this.setState({ amount: e.target.value })} type="number" name="amount" id="amount" placeholder="Amount" min="0" max="5000" step="0.01" required autoComplete="false"/>
            </InputGroup>
          </FormGroup>

          <FormGroup>
            <Label for="detail">
              { <FormattedMessage id="top_up_form.Detail (optional)" defaultMessage="Detail (optional)" /> }
            </Label>
            <Input onChange={(e) => this.setState({ description: e.target.value })} type="textarea" name="detail" id="detail" placeholder="Detail" maxLength="200" autoComplete="false"/>
          </FormGroup>
          
          <Button
            type="submit"
            color="blue-violet"
            className="rounded-pill w-100"
          >
            { <FormattedMessage id="top_up_form.Submit" defaultMessage="Submit" /> }
          </Button>
        </Form>
      </ModalBox>
    )
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModalTopUp));
