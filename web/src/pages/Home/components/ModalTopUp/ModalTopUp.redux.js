import PropTypes from 'prop-types';
import { pick } from 'lodash';
import walletActions from 'actions/wallet';

const propTypes = {
  history:      PropTypes.object,
  // 
  transaction:  PropTypes.object,
  isTopUp:      PropTypes.bool,
  topUpError:   PropTypes.string,
  //
  toggle:       PropTypes.func,
  doTopUp:      PropTypes.func,
};

const defaultProps = {
  transaction:  {},
  isTopUp:      false,
  topUpError:   '',
  //
  toggle:       () => {},
  doTopUp:      () => {},
};

const mapStateToProps = state => ({
  ...pick(state.walletReducer, [
    'transaction',
    'isTopUp',
    'topUpError',
  ]),
});

const mapDispatchToProps = dispatch => ({
  doTopUp: (data) => dispatch(walletActions.topUp(data)),
});

export {
  defaultProps,
  propTypes,
  mapStateToProps,
  mapDispatchToProps,
};