import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Layout from 'components/Layout';
import { matchPath } from 'react-router';
import {
  defaultProps,
  propTypes,
  mapStateToProps,
  mapDispatchToProps,
} from './Home.redux';
import Header from './components/Header';
import WalletList from './components/WalletList';
import styles from './Home.module.scss';


class Home extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  componentDidMount() {
    const { history } = this.props;
    const { fetchData } = this;
    fetchData();

    history.listen((location, action) => {
      const match = matchPath(location.pathname, { path: '/', exact: true, strict: false });
      match && fetchData();
    });
  }

  fetchData = () => {
    const {
      isGettingProfile,
      isGettingWallets,
      isGettingCurrencies,
      doGetUserProfile,
      doGetUserWallets,
      doGetCurrencies,
    } = this.props;
    
    !isGettingProfile && doGetUserProfile && doGetUserProfile();
    !isGettingWallets && doGetUserWallets && doGetUserWallets();
    !isGettingCurrencies && doGetCurrencies && doGetCurrencies();
  }

  render = () => {
    return (
      <Layout contentClass={styles.content}>
        <Header />
        <WalletList />
      </Layout>
    )
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home));