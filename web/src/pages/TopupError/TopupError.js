import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { get, isEmpty } from 'lodash';
import Layout from 'components/Layout';
import styles from './TopupError.module.scss';

class TopupError extends Component {
  static propTypes = {
    history:  PropTypes.object,
  }

  UNSAFE_componentWillMount = () => {
    const error = get(this.props, 'location.state.error', '');
    if (!error || isEmpty(error)) {
      const { history } = this.props;
      history.push('/');
    }
  }

  handleOnClickButtonBackToHome = () => {
    const { history } = this.props;
    history.push('/');
  }

  render() {
    return (
      <Layout contentClass={styles.content}>
        <h2 className={styles.pageTitle}>FAILURE</h2>
        <div className="mb-5">
          <p className={styles.errorMessage}>An error has occurred</p>
          <p className={styles.errorMessage}>Please contact Customer Support</p>
        </div>

        <Button
          type="submit"
          color="blue-violet"
          size="lg"
          className="rounded-pill w-100"
          onClick={() => this.handleOnClickButtonBackToHome()}
        >
          Back to Home
        </Button>
      </Layout>
    )
  }
}

export default withRouter(TopupError);