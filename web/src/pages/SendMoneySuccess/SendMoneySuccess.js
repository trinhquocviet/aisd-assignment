import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import classnames from 'classnames';
import { get } from 'lodash';
import moment from 'moment';
import Layout from 'components/Layout';
import styles from './SendMoneySuccess.module.scss';

class SendMoneySuccess extends Component {
  static propTypes = {
    history:  PropTypes.object,
  }

  UNSAFE_componentWillMount = () => {
    const transactions = get(this.props, 'location.state.transactions', []);
    if (!transactions || transactions.length <= 1) {
      const { history } = this.props;
      history.push('/');
    }
  }

  handleOnClickButtonBackToHome = () => {
    const { history } = this.props;
    history.push('/');
  }

  renderReceipt = (transaction) => {
    return (
      <div key={transaction.tx} className={classnames([styles.receipt, 'mb-2'])}>
        <h4 className={styles.receiptTitle}>RECEIPT</h4>

        <hr className={styles.horizontalDivider} />

        <div className="d-flex mb-3">
          <div className="text-left">Tx ref.</div>
          <div className="text-right flex-grow-1">
            <b>{ transaction.tx }</b>
          </div>
        </div>

        <div className="d-flex mb-3">
          <div className="text-left">Date</div>
          <div className="text-right flex-grow-1">
            <b>{ moment(transaction.createdAt).format("YYYY-MM-DD h:mm:ss") }</b>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const transactions = get(this.props, 'location.state.transactions', []);

    return (
      <Layout contentClass={styles.content}>
        <h2 className={styles.pageTitle}>SUCCESS</h2>
        
        { transactions.map(this.renderReceipt) }

        <Button
          type="submit"
          color="blue-violet"
          size="lg"
          className="rounded-pill w-100"
          onClick={() => this.handleOnClickButtonBackToHome()}
        >
          Back to Home
        </Button>
      </Layout>
    )
  }
}

export default withRouter(SendMoneySuccess);