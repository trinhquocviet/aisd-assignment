import TYPES from 'common/constants/actionTypes';
import localeActions from 'actions/locale';
import { DEFAULT_LOCALE } from 'common/constants/env';

const INITIAL_STATE = {
  locale: localeActions.get() || DEFAULT_LOCALE
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case TYPES.LOCALE.CHANGE_LANG:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
}
