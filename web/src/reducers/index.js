import { combineReducers } from 'redux';
// 
import localeReducer from './locale';
import userReducer from './user';
import currencyReducer from './currency';
import walletReducer from './wallet';
import exchangeReducer from './exchange';

export default combineReducers({
  localeReducer,
  userReducer,
  currencyReducer,
  walletReducer,
  exchangeReducer,
});
