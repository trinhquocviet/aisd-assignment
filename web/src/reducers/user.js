import TYPES from 'common/constants/actionTypes';
import { getReducerData, removeTypeState } from 'common/helpers/reducer';

const INITIAL_STATE = {
  profile:          {},
  wallets:          [],

  isGettingProfile: false,
  isGettingWallets: false,

  getProfileError:  '',
  getWalletsError:  '',
};

export default (state = INITIAL_STATE, action) => {
  const {
    GET_PROFILE,
    GET_WALLETS,
  } = TYPES.USER;
  const type = removeTypeState(action.type);
  // 
  switch (type) {
    case GET_PROFILE:
      return getReducerData(state, action, GET_PROFILE, {
        loading: 'isGettingProfile',
        error: 'getProfileError',
      });
    case GET_WALLETS:
      return getReducerData(state, action, GET_WALLETS, {
        loading: 'isGettingWallets',
        error: 'getWalletsError',
      });
    default:
      return state;
  }
};
