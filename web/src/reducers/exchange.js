import TYPES from 'common/constants/actionTypes';
import { getReducerData, removeTypeState } from 'common/helpers/reducer';

const INITIAL_STATE = {
  rate:               '0',
  amount:             '0',
  requestTime:        0,

  isGettingRate:      false,
  isCalculatingRate:  false,

  getRateError:       '',
  calculateRateError: '',
};

export default (state = INITIAL_STATE, action) => {
  const {
    GET_RATE,
    CALCULATE_RATE
  } = TYPES.EXCHANGE;
  const type = removeTypeState(action.type);
  // 
  switch (type) {
    case GET_RATE:
      return getReducerData(state, action, GET_RATE, {
        loading: 'isGettingRate',
        error: 'getRateError',
      });
    case CALCULATE_RATE:
      return getReducerData(state, action, CALCULATE_RATE, {
        loading: 'isSendingMoney',
        error: 'sendMoneyError',
      });
    default:
      return state;
  }
};
