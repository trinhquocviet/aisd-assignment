import TYPES from 'common/constants/actionTypes';
import { getReducerData, removeTypeState } from 'common/helpers/reducer';

const INITIAL_STATE = {
  currencies:          [],

  isGettingCurrencies: false,

  getCurrenciesError:  '',
};

export default (state = INITIAL_STATE, action) => {
  const {
    GET_LIST,
  } = TYPES.CURRENCY;
  const type = removeTypeState(action.type);
  // 
  switch (type) {
    case GET_LIST:
      return getReducerData(state, action, GET_LIST, {
        loading: 'isGettingCurrencies',
        error: 'getCurrenciesError',
      });
    default:
      return state;
  }
};
