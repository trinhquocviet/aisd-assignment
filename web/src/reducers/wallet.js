import TYPES from 'common/constants/actionTypes';
import { getReducerData, removeTypeState } from 'common/helpers/reducer';

const INITIAL_STATE = {
  transaction:    {},
  transactions:   [],

  isTopUp:        false,
  isSendingMoney: false,

  topUpError:     '',
  sendMoneyError: ''
};

export default (state = INITIAL_STATE, action) => {
  const {
    TOP_UP,
    SEND_MONEY
  } = TYPES.WALLET;
  const type = removeTypeState(action.type);
  // 
  switch (type) {
    case TOP_UP:
      return getReducerData(state, action, TOP_UP, {
        loading: 'isTopUp',
        error: 'topUpError',
      });
    case SEND_MONEY:
      return getReducerData(state, action, SEND_MONEY, {
        loading: 'isSendingMoney',
        error: 'sendMoneyError',
      });
    default:
      return state;
  }
};
