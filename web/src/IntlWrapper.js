import React, { Fragment, PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { isEmpty, pick } from 'lodash';
import { IntlProvider } from 'react-intl';
import { DEFAULT_LOCALE } from 'common/constants/env';
import { COUNTRIES } from 'common/constants/countries';
import intl from 'common/intl';
import localeAction from 'actions/locale';

const LOCALES = COUNTRIES.reduce((prevResult = [], country) => {
  prevResult.push(country.code);
  return prevResult;
}, []);

const parseLocaleCode = (locale) => `en-${locale.toUpperCase()}`;

class IntlWrapper extends PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    locale: PropTypes.oneOf(LOCALES),
    // 
    doUpdateLocale: PropTypes.func,
  };

  static defaultProps = {
    locale: DEFAULT_LOCALE,
    translateMessage: {},
    // 
    doUpdateLocale: () => {},
  };

  state = {
    locale: DEFAULT_LOCALE,
  };

  static getDerivedStateFromProps(props, state) {
    let newState = {};

    const [newLocale, oldLocale] = [props.locale, state.locale];
    if (newLocale !== oldLocale) {
      newState.locale = newLocale;
    }

    return (isEmpty(newState)) ? null : newState;
  }

  componentDidMount() {
    const locale = localeAction.get();
    if (isEmpty(locale)) {
      const { doUpdateLocale } = this.props;
      doUpdateLocale();
    }
  }

  render() {
    const { children } = this.props;
    const { locale } = this.state;
    return (
      <IntlProvider
        key={parseLocaleCode(locale)}
        locale={parseLocaleCode(locale)}
        defaultLocale={parseLocaleCode(locale)}
        messages={intl[locale] || intl[DEFAULT_LOCALE]}
        textComponent={Fragment}
        onError={(str) => console.error(`IntlProvider error: ${str}`)}
      >
        { children }
      </IntlProvider>
    );
  }
}

const mapStateToProps = state => ({
  ...pick(state.localeReducer, [
    'locale',
  ]),
});

const mapDispatchToProps = dispatch => ({
  doUpdateLocale: (locale) => dispatch(localeAction.update(locale)),
});

export default connect(mapStateToProps, mapDispatchToProps)(IntlWrapper);