import React from 'react';
import {
  Switch,
  Route,
  withRouter,
} from 'react-router-dom';
import { ROUTES } from 'common/constants/routes';
import {
  Home,
  NotFound,
  TopupSuccess,
  TopupError,
  SendMoneySuccess,
  SendMoneyError,
} from 'pages';

const Routes = () => (
  <Switch>
    <Route exact path={ROUTES.TOPUP_ERROR.link} component={TopupError} />
    <Route exact path={ROUTES.TOPUP_SUCCESS.link} component={TopupSuccess} />
    <Route exact path={ROUTES.EXCHANGE_ERROR.link} component={SendMoneyError} />
    <Route exact path={ROUTES.EXCHANGE_SUCCESS.link} component={SendMoneySuccess} />
    <Route exact path="/" component={Home} />
    <Route path="*" component={NotFound} />
  </Switch>
);

export default withRouter(Routes);
