export const COUNTRIES = [
  {
    code: 'sg',
    name: 'singapore',
    currency_code: 'SGD',
    currency_symbol: "S$",
  },
  {
    code: 'us',
    name: 'united states',
    currency_code: 'USD',
    currency_symbol: "$",
  },
  {
    code: 'my',
    name: 'malaysia',
    currency_code: 'MYR',
    currency_symbol: "RM",
  },
];
