import buildUrl from 'build-url';
// ---
const BASE_URL = buildUrl(process.env.REACT_APP_API);

// this block define all url link
// ? URL is fixed version will no need version
const API_GET_PROFILE = buildUrl(BASE_URL, { path: 'getProfile' });
const API_GET_WALLETS = buildUrl(BASE_URL, { path: 'getMyWallets' });
const API_GET_CURRENCIES = buildUrl(BASE_URL, { path: 'getLookUpValues' });
const API_GET_RATE = buildUrl(BASE_URL, { path: 'getRate' });
const API_CALCULATE_RATE = buildUrl(BASE_URL, { path: 'calculateRate' });
const API_TOP_UP = buildUrl(BASE_URL, { path: 'TopUp' });
const API_SEND_MONEY = buildUrl(BASE_URL, { path: 'SendMoney' });

// ? URL is NOT fixed version

// ? ...


export {
  API_GET_PROFILE,
  API_GET_WALLETS,
  API_GET_CURRENCIES,
  API_GET_RATE,
  API_CALCULATE_RATE,
  API_TOP_UP,
  API_SEND_MONEY,
};
