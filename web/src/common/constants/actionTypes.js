import { values } from 'lodash';
import { flatten } from 'common/helpers/object';

const TYPES = {
  LOCALE: {
    REGISTER_LANG: 'REGISTER_LANG',
    CHANGE_LANG: 'CHANGE_LANG',
  },
  USER: {
    GET_PROFILE: 'USER_GET_PROFILE',
    GET_WALLETS: 'USER_GET_WALLETS',
  },
  CURRENCY: {
    GET_LIST: 'GET_LIST_CURRENCY',
  },
  EXCHANGE: {
    GET_RATE: 'GET_EXCHANGE_RATE',
    CALCULATE_RATE: 'CALCULATE_EXCHANGE_RATE',
  },
  WALLET: {
    TOP_UP: 'TOP_UP_TO_WALLET',
    SEND_MONEY: 'SEND_MONEY',
  }
}

export default TYPES;
export const ACTION_KEYS = values(flatten(TYPES));
