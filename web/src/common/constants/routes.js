const ROUTES = {
  TOPUP_ERROR: {
    key: 'topup_error',
    link: '/TopupError',
    name: 'Topup Error',
  },
  TOPUP_SUCCESS: {
    key: 'topup_success',
    link: '/TopupSuccess',
    name: 'Topup Success',
  },
  EXCHANGE_ERROR: {
    key: 'exchange_error',
    link: '/SendMoneyError',
    name: 'Send Money Error',
  },
  EXCHANGE_SUCCESS: {
    key: 'exchange_success',
    link: '/SendMoneySuccess',
    name: 'Send Money Success',
  },
};

export {
  ROUTES,
};
