import axios from 'axios';
import { HTTP_TIMEOUT, DEFAULT_LOCALE } from 'common/constants/env';
import localeActions from 'actions/locale';

const httpClient = axios.create();
httpClient.defaults.timeout = HTTP_TIMEOUT;

const defaultHeaders = () => ({
  'country': (localeActions.get() || DEFAULT_LOCALE).toUpperCase(),
  'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkWTf-V867Uqqn68lo8zBju9-qNUgun47A',
});

export default {
  GET: async (url, customHeaders = {}) => {
    let result = null;

    await httpClient
      .get(url, {
        headers: {
          ...defaultHeaders(),
          ...customHeaders,
        },
      })
      .then((response) => {
        result = response;
        return result;
      })
      .catch((error) => {
        result = error.response;
      });

    return result;
  },

  POST: async (url, params, customHeaders = {}) => {
    let result = null;

    await httpClient
      .post(url, params, {
        headers: {
          ...defaultHeaders(),
          ...customHeaders,
        },
      })
      .then((response) => {
        result = response;
        return result;
      })
      .catch((error) => {
        result = error.response;
      });

    return result;
  },

  PUT: async (url, params, customHeaders = {}) => {
    let result = null;

    await httpClient
      .put(url, params, {
        headers: {
          ...defaultHeaders(),
          ...customHeaders,
        },
      })
      .then((response) => {
        result = response;
        return result;
      })
      .catch((error) => {
        result = error.response;
      });

    return result;
  },

  PATCH: async (url, params, customHeaders = {}) => {
    let result = null;

    await httpClient
      .patch(url, params, {
        headers: {
          ...defaultHeaders(),
          ...customHeaders,
        },
      })
      .then((response) => {
        result = response;
        return result;
      })
      .catch((error) => {
        result = error.response;
      });

    return result;
  },

  DELETE: async (url, customHeaders = {}) => {
    let result = null;

    await httpClient
      .delete(url, {
        headers: {
          ...defaultHeaders(),
          ...customHeaders,
        },
      })
      .then((response) => {
        result = response;
        return result;
      })
      .catch((error) => {
        result = error.response;
      });

    return result;
  },
};
