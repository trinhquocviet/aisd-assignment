import { flatten } from 'common/helpers/object';

const messages = flatten({
  'top up': 'tambah nilai',
  'exchange': 'pertukaran',
  'profiles': 'profil',
  'receipt': 'resit',
  'tx ref.': 'tx ref.',
  'date': 'tarikh',
  'back to home': 'kembali ke halaman rumah',
  'failure': 'kegagalan',
  'an error has occurred': 'ralat telah berlaku',
  'please contact customer support': 'sila hubungi sokongan pelanggan',
  'top_up_form': {
    'Currency / Amount': 'Mata Wang / Amaun',
    'Detail (optional)': 'Detail (pilihan)',
    'Detail': 'Terperinci',
    'Submit': 'Hantar',
  },
  'exchange_form': {
    'I want to exchange': 'Saya mahu bertukar',
    'i_want_to_exchange_from_to': `Saya ingin menukar <b>{fromCurrencyCode}</b> ke <b>{toCurrencyCode}</b>`,
    'i_want_to_receive_from_to': `Saya mahu terima <b>{fromCurrencyCode}</b> dari <b>{toCurrencyCode}</b>`,
    'To': 'Dari',
    'Exchange Rate': 'Kadar Pertukaran',
    'Exchange Now': 'Pertukaran Sekarang',
    'Exchange Info': 'Maklumat Pertukaran',
    'Calculate Rate': 'Kira Kadar',
    'send_total_amount': `Hantar jumlah {amount} {currencyCode}`,
    'exchange_rate_text': `<b>1</b> {fromCurrencyCode} equal <i>{rate}</i> {toCurrencyCode}`,
    'exchange_rate_text_default_error': `Tidak boleh mendapatkan atau mengira kadar pertukaran, sila cuba lagi. (Nota: dari-mata wang harus menjadi perbezaan)`,
  },
});
export default messages;