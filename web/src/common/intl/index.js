import sg from './sg';
import us from './us';
import my from './my';

export default {
  us,
  sg,
  my,
};