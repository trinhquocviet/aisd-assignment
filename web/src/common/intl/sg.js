import { flatten } from 'common/helpers/object';

const messages = flatten({
  'top up': 'top up',
  'exchange': 'exchange',
  'profiles': 'profiles',
  'receipt': 'receipt',
  'tx ref.': 'tx ref.',
  'date': 'date',
  'back to home': 'back to home',
  'failure': 'failure',
  'an error has occurred': 'an error has occurred',
  'please contact customer support': 'please contact customer support',
  'top_up_form': {
    'Currency / Amount': 'Currency / Amount',
    'Detail (optional)': 'Detail (optional)',
    'Detail': 'Detail',
    'Submit': 'Submit',
  },
  'exchange_form': {
    'I want to exchange': 'I want to exchange',
    'i_want_to_exchange_from_to': `I want to exchange <b>{fromCurrencyCode}</b> to <b>{toCurrencyCode}</b>`,
    'i_want_to_receive_from_to': `I want to receive <b>{fromCurrencyCode}</b> from <b>{toCurrencyCode}</b>`,
    'To': 'To',
    'Exchange Rate': 'Exchange Rate',
    'Exchange Now': 'Exchange Now',
    'Exchange Info': 'Exchange Info',
    'Calculate Rate': 'Calculate Rate',
    'send_total_amount': `Send total {amount} {currencyCode}`,
    'exchange_rate_text': `<b>1</b> {fromCurrencyCode} equal <i>{rate}</i> {toCurrencyCode}`,
    'exchange_rate_text_default_error': `Can't get or calculate the exchange rate, please try again. (Note: from-to currencies should be a difference)`,
  },
});
export default messages;