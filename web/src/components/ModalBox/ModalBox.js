import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Modal,
  ModalHeader,
  ModalBody,
} from 'reactstrap';
import classnames from 'common/helpers/classnames';
import styles from './ModalBox.module.scss';

export default class ModalBox extends Component {
  static propTypes = {
    children:         PropTypes.node.isRequired,
    title:            PropTypes.string,
    formattedMessage: PropTypes.object,
    className:        PropTypes.string,
    // 
    size:             PropTypes.string,
    isOpen:           PropTypes.bool,
    // 
    toggle:           PropTypes.func,
    onClosed:         PropTypes.func,
  }

  static defaultProps = {
    title:            "",
    formattedMessage: null,
    // 
    size:             "md",
    isOpen:           false,
    // 
    toggle:           () => {},
    onClosed:         () => {},
  }

  closeBtn = () => {
    const { toggle } = this.props;
    return (
      <button className={styles.closeBtn} onClick={toggle}>
        <i className="fas fa-times" />
      </button>
    );
  }

  render() {
    const {
      title,
      formattedMessage,
      children,
      toggle,
      className,
      isOpen,
      size,
      onClosed,
    } = this.props;

    return (
      <Modal
        centered
        size={size}
        isOpen={isOpen}
        backdrop="static"
        scrollable={false}
        onClosed={onClosed}
        className={classnames([styles.wrapper, className])}
      >
        <ModalHeader
          toggle={toggle}
          close={this.closeBtn()}
          className={styles.header}
        >
          { title || formattedMessage || '' }
        </ModalHeader>
        <ModalBody
          className={styles.body}
        >
          { children || null }
        </ModalBody>
      </Modal>
    )
  }
}
