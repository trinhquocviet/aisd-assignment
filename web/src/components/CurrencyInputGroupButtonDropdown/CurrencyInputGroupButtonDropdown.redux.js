import PropTypes from 'prop-types';
import { pick } from 'lodash';

const propTypes = {
  addonType:            PropTypes.string,
  displayType:          PropTypes.oneOf(['flag', 'name', 'flag-name']),
  selectedCurrencyCode: PropTypes.string,
  // 
  currencies:           PropTypes.array,
  // 
  onChange:             PropTypes.func,
};

const defaultProps = {
  addonType:            'append',
  displayType:          'flag-name',
  selectedCurrencyCode: '',
  // 
  currencies:           [],
  // 
  onChange:             () => {}
};

const mapStateToProps = state => ({
  ...pick(state.currencyReducer, [
    'currencies',
  ]),
});

export {
  defaultProps,
  propTypes,
  mapStateToProps,
};