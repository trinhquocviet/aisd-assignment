import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  InputGroupButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import { isEmpty } from 'lodash';
import classnames from 'common/helpers/classnames';
import { DEFAULT_LOCALE } from 'common/constants/env';
import { COUNTRIES } from 'common/constants/countries';
import {
  defaultProps,
  propTypes,
  mapStateToProps,
} from './CurrencyInputGroupButtonDropdown.redux';
import styles from './CurrencyInputGroupButtonDropdown.module.scss';

class CurrencyInputGroupButtonDropdown extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  state = {
    isOpen: false,
    selectedCountry: COUNTRIES.find(c => c.code === DEFAULT_LOCALE),
    selectedCurrencyCode: DEFAULT_LOCALE,
  }

  static getDerivedStateFromProps(props, state) {
    let newState = {};
    const [selectedCurrencyCode, oldSelectedCurrencyCode] = [props.selectedCurrencyCode, state.selectedCurrencyCode];
    if (!isEmpty(selectedCurrencyCode) && selectedCurrencyCode !== oldSelectedCurrencyCode) {
      const country = COUNTRIES.find(c => c.currency_code === selectedCurrencyCode);
      if (country) {
        newState.selectedCountry = country;
        newState.selectedCurrencyCode = selectedCurrencyCode;
      }
    }

    return (isEmpty(newState)) ? null : newState;
  }

  componentDidMount() {
    const { currencies, onChange } = this.props;
    const selectedCountry = COUNTRIES.find(c => c.code === DEFAULT_LOCALE);
    
    this.setState({ selectedCountry }, () => {
      const temp = currencies.find(c => c.code === selectedCountry.currency_code);
      if (temp && temp.currencyID) {
        onChange && onChange(temp.currencyID);
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const [oldSelectedCountry, newSelectedCountry] = [prevState.selectedCountry, this.state.selectedCountry];
    if (oldSelectedCountry.code !== newSelectedCountry.code) {
      const { currencies, onChange } = this.props;
      const temp = currencies.find(c => c.code === newSelectedCountry.currency_code);
      onChange(temp.currencyID);
    }
  }
  
  handleOnChangeValue = (country) => {
    // const { currencies, onChange } = this.props;
    this.setState({ selectedCountry: country });
    // this.setState({ selectedCountry: country }, () => {
    //   const temp = currencies.find(c => c.code === country.currency_code);
    //   console.log(currencies);
    //   if (temp && temp.currencyID) {
    //     onChange && onChange(temp.currencyID);
    //   }
    // });
    
  }

  renderDropdownToggleContent = () => {
    const { displayType } = this.props;
    const { selectedCountry } = this.state;
    const { code, name } = selectedCountry;

    const flagComponent = (<span className={classnames([`flag-icon flag-icon-${code}`, styles.countryFlag])} />);
    const nameComponent = (<span className={styles.countryName}>{ name }</span>);

    return (
      <span className={styles.countryContent}>
        { displayType.includes('flag') ? flagComponent : null }
        { displayType.includes('name') ? nameComponent : null }
      </span>
    );
  }

  renderDropdownItem = (country) => {
    const { code, name } = country;

    return (
      <DropdownItem
        key={code}
        className={styles.dropdownItem}
        onClick={() => this.handleOnChangeValue(country)}
      >
        <span className={styles.countryContent}>
          <span className={classnames([`flag-icon flag-icon-${code}`, styles.countryFlag])} />
          <span className={styles.countryName}>{ name }</span>
        </span>
      </DropdownItem>
    );
  }

  render() {
    const {
      addonType,
    } = this.props;

    const {
      isOpen,
    } = this.state;

    return (
      <InputGroupButtonDropdown
        addonType={addonType}
        isOpen={isOpen}
        toggle={() => this.setState({ isOpen: !isOpen })}
      >
        <DropdownToggle caret className={styles.dropdownToggle}>
          { this.renderDropdownToggleContent() }
        </DropdownToggle>
        <DropdownMenu>
          { COUNTRIES.map(this.renderDropdownItem) }
        </DropdownMenu>
      </InputGroupButtonDropdown>
    )
  }
}

export default connect(mapStateToProps, null)(CurrencyInputGroupButtonDropdown);