import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import classnames from 'common/helpers/classnames';
import { COUNTRIES } from 'common/constants/countries';
import {
  defaultProps,
  propTypes,
  mapStateToProps,
  mapDispatchToProps,
} from './CountrySwitcher.redux';
import styles from './CountrySwitcher.module.scss';

class CountrySwitcher extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  state = {
    isOpen: false,
  }

  handleToggle = (isOpen) => this.setState({ isOpen });
  handleChangeCountry = (countryCode) => {
    const { doUpdateLocale } = this.props;
    if (doUpdateLocale) {
      doUpdateLocale(countryCode);
    }
  }

  renderCountryContent = (country) => {
    return (
      <span className={styles.countryContent}>
        <span
          className={classnames(['flag-icon', `flag-icon-${country.code}`, styles.countryFlag])}
        />
        <span className={styles.countryName}>{ country.name }</span>
      </span>
    );
  }

  renderSelectedCountry = () => {
    const { locale } = this.props;
    const country = COUNTRIES.find(c => c.code === locale);
    return (
      <DropdownToggle caret className={styles.toggler}>
        { this.renderCountryContent(country) }
      </DropdownToggle>
    )
  }

  renderDropdownItem = () => {
    const { locale } = this.props;
    const countriesFiltered = COUNTRIES.filter(c => c.code !== locale);

    return countriesFiltered.map(c => (
      <DropdownItem
        key={c.code}
        className={styles.dropdownItem}
        onClick={() => this.handleChangeCountry(c.code)}
      >
        { this.renderCountryContent(c) }
      </DropdownItem>
    ))
  }

  render() {
    const { className } = this.props;
    const { isOpen } = this.state;

    return (
      <ButtonDropdown
        isOpen={isOpen}
        className={classnames([styles.wrapper, className])}
        toggle={() => this.handleToggle(!isOpen)}
      >
        { this.renderSelectedCountry() }
        <DropdownMenu className={styles.dropdownMenu}>
          { this.renderDropdownItem() }
        </DropdownMenu>
      </ButtonDropdown>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CountrySwitcher);