import PropTypes from 'prop-types';
import { pick } from 'lodash';
import { DEFAULT_LOCALE } from 'common/constants/env';
import localeActions from 'actions/locale';

const propTypes = {
  className:      PropTypes.string,
  // 
  locale:         PropTypes.string,
  // 
  doUpdateLocale: PropTypes.func,
};

const defaultProps = {
  className:      '',
  // 
  locale:         DEFAULT_LOCALE,
  // 
  doUpdateLocale: () => {},
};

const mapStateToProps = state => ({
  ...pick(state.localeReducer, [
    'locale',
  ]),
});

const mapDispatchToProps = dispatch => ({
  doUpdateLocale: (locale) => dispatch(localeActions.update(locale)),
});

export {
  defaultProps,
  propTypes,
  mapStateToProps,
  mapDispatchToProps,
};