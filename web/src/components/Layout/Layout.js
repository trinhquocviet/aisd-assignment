import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'common/helpers/classnames';
import { Scrollbars } from 'react-custom-scrollbars';
import Navigation from './components/Navigation';
import Loading from './components/Loading';
import styles from './Layout.module.scss';

export default class Layout extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    containerClass: PropTypes.string,
    contentClass: PropTypes.string,
  };

  static defaultProps = {
    containerClass: '',
    contentClass: '',
  };

  render = () => {
    const {
      children,
      contentClass,
      containerClass,
    } = this.props;

    return (
      <Scrollbars
        autoHide
        className={styles.wrapper}
      >
        <div className={styles.header}>
          <Navigation className={styles.navigation} />
        </div>
        <div className={classNames([styles.container, containerClass])}>
          <div className={classNames([styles.content, contentClass])}>
            {children}
          </div>
        </div>
        <Loading />
      </Scrollbars>
    )
  }
}
