import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { pick, get } from 'lodash';
import { FormattedMessage } from 'react-intl';
import ModalBox from 'components/ModalBox';

class ModalUser extends Component {
  static propTypes = {
    isOpen:       PropTypes.bool,
    profile:      PropTypes.object,
    toggle:       PropTypes.func,
  };
  
  static defaultProps = {
    isOpen:       false,
    profile:      {},
    toggle:       () => {},
  };

  render() {
    const { profile } = this.props;
    return (
      <ModalBox
        size="sm"
        formattedMessage={<FormattedMessage id="profiles" defaultMessage="profiles" />}
        isOpen={this.props.isOpen}
        toggle={this.props.toggle}
      >
        <h6 className="text-center">
          { get(profile, 'Name', '--') }
        </h6>

        <p className="text-center mb-1">
          { get(profile, 'Email', '--') }
        </p>

        <p className="text-center mb-0">
          { get(profile, 'CreatedAt', '') }
        </p>
      </ModalBox>
    )
  }
}

const mapStateToProps = state => ({
  ...pick(state.userReducer, [
    'profile',
  ]),
});

export default connect(mapStateToProps, null)(ModalUser);