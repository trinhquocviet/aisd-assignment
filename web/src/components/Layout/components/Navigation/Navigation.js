import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Row, Col,
} from 'reactstrap';
import classnames from 'common/helpers/classnames';
import CountrySwitcher from 'components/CountrySwitcher';
import brandIcon from 'common/assets/images/brand/brand-icon.svg';
import brandFull from 'common/assets/images/brand/brand-full.svg';
import ModalUser from '../ModalUser';
import styles from './Navigation.module.scss';

export default class Navigation extends Component {
  static propTypes = {
    className: PropTypes.string,
  }

  static defaultProps = {
    className: '',
  }

  state = {
    showModalUser: false,
  }

  render() {
    const {
      className,
    } = this.props;

    const {
      showModalUser,
    } = this.state;

    return (
      <Navbar className={classnames([styles.wrapper, className])} fixed="top">
        <Row className="w-100 align-items-center">
          <Col className="text-left">
            <NavbarToggler
              className={styles.toggler}
              onClick={() => this.setState({ showModalUser: !showModalUser })}
            />
            <ModalUser
              isOpen={showModalUser}
              toggle={() => this.setState({ showModalUser: !showModalUser })}
            />
          </Col>
          <Col className="text-center">
            <NavbarBrand className={styles.brand} href="/">
              <img className={styles.brandIcon} src={brandIcon} alt="brand" />
              <img className={styles.brandFull} src={brandFull} alt="brand" />
            </NavbarBrand>
          </Col>
          <Col className="text-right">
            <CountrySwitcher />
          </Col>
        </Row>
      </Navbar>
    )
  }
}
