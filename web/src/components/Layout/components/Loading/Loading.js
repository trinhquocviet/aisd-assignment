import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import classnames from "common/helpers/classnames";
import {
  defaultProps,
  propTypes,
  mapStateToProps,
} from './Loading.redux';
import styles from './Loading.module.scss';


class Loading extends PureComponent {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  isLoading = () => {
    const {
      isGettingProfile,
      isGettingWallets,
      isGettingCurrencies,
      isTopUp,
      isSendingMoney,
      isCalculateRate,
    } = this.props;

    return (
        isGettingProfile
      ||isGettingWallets
      ||isGettingCurrencies
      ||isTopUp
      ||isSendingMoney
      ||isCalculateRate
      );
  }

  getExtClasses = () => this.isLoading() ? 'd-flex' : 'd-none';

  render = () => {
    const extClass = this.getExtClasses();
    return (
      <div className={classnames([styles.wrapper, extClass])}>
        <div className={styles.loading}></div>
        <img className={styles.logo} src="/brand.svg" alt="EWallet" />
      </div>
    );
  } 
}

export default connect(mapStateToProps, null)(Loading);