import PropTypes from 'prop-types';
import { pick } from 'lodash';

const propTypes = {
  isGettingProfile:     PropTypes.bool,
  isGettingWallets:     PropTypes.bool,
  isGettingCurrencies:  PropTypes.bool,
  isTopUp:              PropTypes.bool,
  isSendingMoney:       PropTypes.bool,
  isCalculateRate:      PropTypes.bool,
};

const defaultProps = {
  isGettingProfile:     false,
  isGettingWallets:     false,
  isGettingCurrencies:  false,
  isTopUp:              false,
  isSendingMoney:       false,
  isCalculateRate:      false,
};

const mapStateToProps = state => ({
  ...pick(state.userReducer, [
    'isGettingProfile',
    'isGettingWallets',
  ]),
  ...pick(state.currencyReducer, [
    'isGettingCurrencies'
  ]),
  ...pick(state.exchangeReducer, [
    'isCalculateRate'
  ]),
  ...pick(state.walletReducer, [
    'isTopUp',
    'isSendingMoney'
  ]),
});

export {
  defaultProps,
  propTypes,
  mapStateToProps,
};