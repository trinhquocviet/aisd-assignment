import axios from 'common/helpers/axios';
// import buildUrl from 'build-url';
import {
  API_GET_WALLETS,
  API_GET_PROFILE,
} from 'common/constants/api';
import TYPES from 'common/constants/actionTypes';
import { actionCallAPI } from 'common/helpers/action';

const userActions = {
  getProfile: () => (dispatch) => {
    const actionName = TYPES.USER.GET_PROFILE;
    const asyncAction = axios.GET(API_GET_PROFILE);
  
    const actions = {
      success: async (response) => {
        if (response.status === 200) {
          const responseData = response.data || {};
          return {
            profile: responseData.Profile || {},
          };
        }
        return null;
      },
      error: (response) => {
        return response.data.error || `${response.status}`;
      }
    };
  
    return dispatch(actionCallAPI(actionName, asyncAction, actions));
  },
  getWallets: () => (dispatch) => {
    const actionName = TYPES.USER.GET_WALLETS;
    const asyncAction = axios.GET(API_GET_WALLETS);
  
    const actions = {
      success: async (response) => {
        if (response.status === 200) {
          const responseData = response.data || {};
          return {
            wallets: responseData.Wallets || {},
          };
        }
        return null;
      },
      error: (response) => {
        return response.data.error || `${response.status}`;
      }
    };
  
    return dispatch(actionCallAPI(actionName, asyncAction, actions));
  },
}

export default userActions;