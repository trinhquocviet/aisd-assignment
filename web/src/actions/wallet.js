import axios from 'common/helpers/axios';
import {
  API_TOP_UP,
  API_SEND_MONEY,
} from 'common/constants/api';
import TYPES from 'common/constants/actionTypes';
import { actionCallAPI } from 'common/helpers/action';

const walletActions = {
  topUp: (data) => (dispatch) => {
    const actionName = TYPES.WALLET.TOP_UP;
    const asyncAction = axios.POST(API_TOP_UP, data);
  
    const actions = {
      success: async (response) => {
        if (response.status === 200) {
          const responseData = response.data || {};
          let transaction = {};
          if (responseData.Transaction) {
            transaction = {
              topupID: responseData.Transaction.TopUpID,
              tx: responseData.Transaction.TX,
              status: responseData.Transaction.Status,
              createdAt: responseData.Transaction.CreatedAt
            };
          }
          
          return { transaction };
        }
        return null;
      },
      error: (response) => {
        return response.data.error || `${response.status}`;
      }
    };
  
    return dispatch(actionCallAPI(actionName, asyncAction, actions));
  },
  sendMoney: (data) => (dispatch) => {
    const actionName = TYPES.WALLET.SEND_MONEY;
    const asyncAction = axios.POST(API_SEND_MONEY, data);
  
    const actions = {
      success: async (response) => {
        if (response.status === 200) {
          const responseData = response.data || {};
          const transactions = (responseData.Transactions || []).reduce((prevResult, res) => {
            prevResult.push({
              id: res.ID,
              tx: res.TX,
              status: res.Status,
              createdAt: res.CreatedAt
            })
            return prevResult;
          }, []);

          return {
            transactions,
          };
        }
        return null;
      },
      error: (response) => {
        return response.data.error || `${response.status}`;
      }
    };
  
    return dispatch(actionCallAPI(actionName, asyncAction, actions));
  },
}

export default walletActions;