import axios from 'common/helpers/axios';
import {
  API_GET_CURRENCIES,
} from 'common/constants/api';
import TYPES from 'common/constants/actionTypes';
import { actionCallAPI } from 'common/helpers/action';

const currencyActions = {
  getLookUpValues: () => (dispatch) => {
    const actionName = TYPES.CURRENCY.GET_LIST;
    const asyncAction = axios.GET(API_GET_CURRENCIES);
  
    const actions = {
      success: async (response) => {
        if (response.status === 200) {
          const responseData = response.data || {};
          const result = (responseData.Currencies || []).reduce((prevResult, c) => {
            prevResult.push({
              currencyID: c.CurrencyID,
              code: c.Name,
            });
            return prevResult;
          }, []);

          return {
            currencies: result,
          };
        }
        return null;
      },
      error: (response) => {
        return response.data.error || `${response.status}`;
      }
    };
  
    return dispatch(actionCallAPI(actionName, asyncAction, actions));
  },
}

export default currencyActions;