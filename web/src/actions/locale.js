import { DEFAULT_LOCALE } from 'common/constants/env';
import TYPES from 'common/constants/actionTypes';
import { Cookies } from 'react-cookie';

const LOCAL_STORAGE_KEYS = 'locale:setting';

const localeActions = {
  get: () => {
    const cookie = new Cookies();
    return cookie.get(LOCAL_STORAGE_KEYS) || '';
  },
  update: (locale = DEFAULT_LOCALE) => async (dispatch) => {
    dispatch({
      type: TYPES.LOCALE.CHANGE_LANG,
      payload: {
        locale,
      }
    });

    const cookie = new Cookies();
    cookie.set(LOCAL_STORAGE_KEYS, locale);
  },
}

export default localeActions;