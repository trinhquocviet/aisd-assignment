import axios from 'common/helpers/axios';
import {
  API_GET_RATE,
  API_CALCULATE_RATE,
} from 'common/constants/api';
import moment from 'moment';
import TYPES from 'common/constants/actionTypes';
import { actionCallAPI } from 'common/helpers/action';

const exchangeActions = {
  getRate: (data, requestTime) => (dispatch) => {
    const actionName = TYPES.EXCHANGE.GET_RATE;
    const asyncAction = axios.POST(API_GET_RATE, data);
  
    const actions = {
      success: async (response) => {
        if (response.status === 200) {
          const responseData = response.data || {};
          
          return {
            rate: responseData.rate || '1',
            requestTime: requestTime || moment().unix(),
            getRateError: '',
          };
        }
        return null;
      },
      error: (response) => {
        return response.data.error || `${response.status}`;
      }
    };
  
    return dispatch(actionCallAPI(actionName, asyncAction, actions));
  },
  calculateRate: (data, requestTime) => (dispatch) => {
    const actionName = TYPES.EXCHANGE.CALCULATE_RATE;
    const asyncAction = axios.POST(API_CALCULATE_RATE, data);
  
    const actions = {
      success: async (response) => {
        if (response.status === 200) {
          const responseData = response.data || {};
          return {
            rate: responseData.data.rate || '1',
            amount: responseData.data.amount || '0',
            requestTime: requestTime || moment().unix(),
            calculateRateError: '',
          };
        }
        return null;
      },
      error: (response) => {
        return response.data.error || `${response.status}`;
      }
    };
  
    return dispatch(actionCallAPI(actionName, asyncAction, actions));
  },
}

export default exchangeActions;