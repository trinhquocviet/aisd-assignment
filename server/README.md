
# Introduction
Requires to install before start:
- [Golang version 1.13.1 or lastest](https://golang.org/dl/)
- [MySQL version 5.7 or lastest](https://www.mysql.com/downloads/)

# Table of Contents
1. [Files & Directories structure](#files--directories-structure)
    - [Project structure](#project-structure)
    - [API package structure](#api-package-structure)
2. [Usage](#usage)
    - [Start Golang server](#start-server)
3. [API endpoints](#api-endpoints)
	  - [1. getProfile](#getProfile)
	  - [2. getMyWallets](#getMyWallets)
	  - [3. getLookUpValues](#getLookUpValues)
	  - [4. getRate](#getRate)
	  - [5. calculateRate (US customer only)](#calculateRate)
	  - [6. TopUp](#TopUp)
    - [7. SendMoney](#SendMoney)
    - [Error response](#error-response)
4. [Datasheet](#datasheet)
---
## Files & Directories structure

Describe for all files and directories structure using in this project.

### Project structure
```
.
├── server
│   ├── api
│   ├── constants
│   ├── internal           # Internal package, only share to internal project
│   ├── migrations         # Contain SQL scripts
│   ├── migrations_test    # Contain SQL scripts include mock-data for testing
│   ├── model
│   ├── routes
│   ├── scripts            # All bashscript to help to run on production
│   ├── Dockerfile
│   ├── go.mod
│   ├── go.sum
│   ├── main.go
│   └── ...
└── ...
```

###  API package structure

- One `api` package can have many handlers.
- One `handler` contains controller - model - repository.
	- Controller will handle request, response and validate.
	- Model contains all structs/interface and mapping.
	- Repository handles the request to get data from database.

```
api
├── controller_base.go     # The controller inside handler should inherit this
├── repository_base.go     # The repository inside handler should inherit this
├── handler
│   ├── controller.go
│   ├── model.go
│   ├── repository.go
└── ...
```

## Usage

Clone this project and put follow this path `$GOPATH/src/projects/challenge`, all codebase should place inside this folder

### Start server
**Required: installed Golang and MySQL**, follow the command below to start the server.

01 - Install libs
```bash
go get -u
```

02 - Generate .env file
```bash
cp .env.sample .env
```

03 - Start server
```bash
export $(grep -v '^#' .env | xargs) && go run main.go
```

**TO RUN ALL THE TESTS**
```bash
./scripts/test.sh
```

##  API endpoints

**NOTE**: All APIs request require header `Authorization`

```
Authorization: Bearer <token>
```

### 1. getProfile
```
GET /getProfile
```
#### Request

(NONE)

#### Response
| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:------------|
| Success    | bool      | yes      |             |
| Profile    | object    | yes      |             |

```json
{
    "Success": true,
    "Profile": {
        "UserID": 1,
        "Name": "Judy Kaszper",
        "Email": "judy@example.com",
        "CreatedAt": "2019-11-15T09:10:40Z"
    }
}
```

---
### 2. getMyWallets

```
GET /getMyWallets
```

#### Request

(NONE)

#### Response

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:------------|
| Success    | bool      | yes      |             |
| Wallets    | []object  | yes      |             |
| Count      | number    | yes      |             |

```json
{
    "Success": true,
    "Wallets": [
        {
            "WalletID": 1,
            "CurrencyID": 1,
            "Amount": "500.00",
            "UpdatedAt": "2019-11-15T09:10:40Z"
        },
        {
            "WalletID": 2,
            "CurrencyID": 2,
            "Amount": "5000.00",
            "UpdatedAt": "2019-11-15T09:10:40Z"
        },
    ],
    "Count": 2
}
```

---
### 3. getLookUpValues

```
GET /getLookUpValues
```

#### Request

(NONE)

#### Response

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:------------|
| Success    | bool      | yes      |             |
| Currencies | []object  | yes      |             |
| Count      | number    | yes      |             |

```json
{
    "Success": true,
    "Currencies": [
        {
            "CurrencyID": 1,
            "Name": "SGD"
        },
        {
            "CurrencyID": 2,
            "Name": "USD"
        },
        {
            "CurrencyID": 3,
            "Name": "MYR"
        }
    ],
    "Count": 3
}
```

---
### 4. getRate

```
POST /getRate
```

#### Request

| Attribute      | Type      | Required | Description |
| :------------- |:---------:|:--------:|:------------|
| fromCurrencyID | number    | yes      |             |
| toCurrencyID   | number    | yes      |             |

```json
{
  "fromCurrencyID": 1,
	"toCurrencyID": 2
}
```

#### Response

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:------------|
| Success    | bool      | yes      |             |
| rate       | string    | yes      |             |

```json
{
    "Success": true,
    "rate": "0.73"
}
```

---
### 5. calculateRate

**NOTE** only support for US customers, add header `country` with value is `US`

```
POST /calculateRate
```

#### Request

| Attribute      | Type      | Required | Description |
| :------------- |:---------:|:--------:|:------------|
| fromCurrencyID | number    | yes      |             |
| toCurrencyID   | number    | yes      |             |
| type           | string    | yes      | only `FROM` |
| amount         | string    | yes      |             |

```json
{
	"fromCurrencyID": 1,
	"toCurrencyID": 2,
	"type": "FROM",
	"amount": "5000"
}
```

#### Response

| Attribute      | Type      | Required | Description |
| :------------- |:---------:|:--------:|:------------|
| Success        | bool      | yes      |             |
| data           | object    | yes      |             |
| data.rate      | string    | yes      |             |
| data.amount    | string    | yes      |             |

```json
{
    "Success": true,
    "data": {
        "rate": "0.73",
        "amount": "3650.00"
    }
}
```

---
### 6. TopUp

```
GET /TopUp
```

#### Request

| Attribute      | Type      | Required | Description |
| :------------- |:---------:|:--------:|:------------|
| currencyID     | number    | yes      |             |
| amount         | string    | yes      |             |
| description    | string    |          |             |
| receipt        | string    |          |             |

```json
{
	"currencyID": 1,
	"amount": "1000",
	"description": "",
	"receipt": ""
}
```

#### Response

| Attribute   | Type      | Required | Description |
| :---------- |:---------:|:--------:|:------------|
| Success     | bool      | yes      |             |
| Transaction | object    | yes      |             |


```json
{
    "Success": true,
    "Transaction": {
        "TopUpID": 3,
        "TX": "TOP5328262559018",
        "Status": "success",
        "CreatedAt": "2019-11-15T10:20:05Z"
    }
}
```

---
### 6. SendMoney

```
GET /SendMoney
```

#### Request

| Attribute      | Type      | Required | Description |
| :------------- |:---------:|:--------:|:------------|
| fromCurrencyID | number    | yes      |             |
| toCurrencyID   | string    | yes      |             |
| type           | string    | yes      | only `FROM` |
| amount         | string    | yes      |             |

```json
{
	"fromCurrencyID": 1,
	"toCurrencyID": 2,
	"type": "FROM",
	"amount": "20"
}
```

#### Response

| Attribute    | Type      | Required | Description |
| :----------- |:---------:|:--------:|:------------|
| Success      | bool      | yes      |             |
| Transactions | [2]object | yes      |             |


```json
{
    "Success": true,
    "Transactions": [
        {
            "ID": 4,
            "TX": "EX82848197515147-1",
            "Status": "success",
            "CreatedAt": "2019-11-15T10:22:20Z"
        },
        {
            "ID": 5,
            "TX": "EX82848197515147-2",
            "Status": "success",
            "CreatedAt": "2019-11-15T10:22:20Z"
        }
    ]
}
```

---
### Error response

Default struct for all error response

#### Response

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:------------|
| Success    | bool      | yes      |             |
| Error      | string    | yes      |             |


```json
{
  "success": false,
  "error": "an error has occurred. please try again later"
}
```

## Datasheet

Below is the list of accounts available to use

| Name               | Email            | Password |
| ------------------ | ---------------- | -------- |
| Judy Kaszper       | judy@example.com |  123456  |
| Richard Bilborough | rich@example.com |  123456  |


```bash
# Judy's Token

eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkWTf-V867Uqqn68lo8zBju9-qNUgun47A
```


```bash
# Richard's Token

eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.GxjYTIKTqnSnjH9w7iMRRM4pUW_SgcwKNxCqEc09z2I
```