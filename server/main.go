package main

import (
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/sandalwing/echo-logrusmiddleware"
	"os"
	"bitbucket.org/trinhquocviet/aisd-assignment/server/routes"
	"strings"
)

func main() {
	e := configEchoFramework()
	routes.SetupAPIRoutes(e)

	// Server
	serverPort := fmt.Sprintf(":%s", os.Getenv("API_PORT"))
	e.Logger.Fatal(e.Start(serverPort))
}

func configEchoFramework() *echo.Echo {
	e := echo.New()
	e.Logger = logrusmiddleware.Logger{logrus.StandardLogger()}
	e.Use(logrusmiddleware.Hook())

	// Middleware
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: strings.Split(os.Getenv("API_HEADER_ALLOW_ORIGINS"), ","),
		AllowMethods: strings.Split(os.Getenv("API_HEADER_ALLOW_METHODS"), ","),
		AllowHeaders: strings.Split(os.Getenv("API_HEADER_ALLOW_HEADERS"), ","),
	}))

	return e
}
