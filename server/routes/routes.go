package routes

import (
	currency "bitbucket.org/trinhquocviet/aisd-assignment/server/api/currency"
	transaction "bitbucket.org/trinhquocviet/aisd-assignment/server/api/transaction"
	user "bitbucket.org/trinhquocviet/aisd-assignment/server/api/user"
	"github.com/labstack/echo"
)

// SetupAPIRoutes is main function to start to setup routes
func SetupAPIRoutes(e *echo.Echo) {
	root := e.Group("")

	userRoutes(root)
	currencyRoutes(root)
	calculateRateRoutes(root)
	transactionRoutes(root)
}

func userRoutes(group *echo.Group) {
	var ctr user.Controller
	ctr.UpdatePrivateRoutes([]string{
		"/getProfile",
		"/getMyWallets",
	})

	group.GET("/getProfile", ctr.GetProfile, ctr.ValidateRoute)
	group.GET("/getMyWallets", ctr.GetUserWalletsByAuthToken, ctr.ValidateRoute)
}

func currencyRoutes(group *echo.Group) {
	var ctr currency.Controller
	ctr.UpdatePrivateRoutes([]string{
		"/getLookUpValues",
		"/getRate",
	})

	group.GET("/getLookUpValues", ctr.GetAvailableCurrencies, ctr.ValidateRoute)
	group.POST("/getRate", ctr.GetRate, ctr.ValidateRoute)
}

func calculateRateRoutes(group *echo.Group) {
	var ctr currency.Controller
	ctr.UpdatePrivateRoutes([]string{
		"/calculateRate",
	})

	// Limit only customer from US can call this API
	calculateRateRoute := group.Group("/calculateRate")
	calculateRateRoute.Use(ctr.ValidateRoute)
	calculateRateRoute.Use(ctr.RestrictUSOnly)
	calculateRateRoute.POST("", ctr.CalculateRate)
}

func transactionRoutes(group *echo.Group) {
	var ctr transaction.Controller
	ctr.UpdatePrivateRoutes([]string{
		"/TopUp",
		"/SendMoney",
	})

	group.POST("/TopUp", ctr.TopUp, ctr.ValidateRoute)
	group.POST("/SendMoney", ctr.SendMoney, ctr.ValidateRoute)
}
