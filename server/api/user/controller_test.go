package user

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"
	"testing"

	msg "bitbucket.org/trinhquocviet/aisd-assignment/server/internal/message"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

func TestController_GetProfile(t *testing.T) {
	successResFormat := `{"Success":true,"Profile":{"Email":"%s"}}`
	erroResFormat := `{"Success":false,"Error":"%s"}`

	tests := []struct {
		name           string
		token          string
		customReqJSON  string
		wantHTTPStatus int
		wantResJSON    string
	}{
		{
			name:           "Query Judy info",
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkWTf-V867Uqqn68lo8zBju9-qNUgun47A",
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    fmt.Sprintf(successResFormat, "judy@example.com"),
		},
		{
			name:           "Query deleted account",
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.GxjYTIKTqnSnjH9w7iMRRM4pUW00gcwKNxCqEc09z2I",
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.UnauthorizedToken),
		},
		{
			name:           "Query account with no token",
			token:          "",
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.UnauthorizedToken),
		},
		{
			name:           "Query don't exist token user",
			token:          "thistokendoesnotexist",
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.UnauthorizedToken),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var ctr Controller
			e := echo.New()
			req := httptest.NewRequest(http.MethodGet, "/getProfile", strings.NewReader(""))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", tt.token))
			rec := httptest.NewRecorder()
			c := e.NewContext(req, rec)
			err := ctr.GetProfile(c)

			actuallyResult := rec.Body.String()
			reStrFirst := regexp.MustCompile(`((UserID\".*\,\"Email))`)
			reStrAfter := regexp.MustCompile(`(\,\"CreatedAt\".*$)`)
			actuallyResult = reStrFirst.ReplaceAllString(actuallyResult, `Email`)
			actuallyResult = reStrAfter.ReplaceAllString(actuallyResult, `}}`)

			if assert.NoError(t, err) {
				assert.Equal(t, tt.wantHTTPStatus, rec.Code)
				assert.Equal(t, tt.wantResJSON, actuallyResult)
			}
		})
	}
}

func TestController_GetUserWalletsByAuthToken(t *testing.T) {
	successResFormat := `{"Success":true,"Wallets":[],"Count":%d}`
	erroResFormat := `{"Success":false,"Error":"%s"}`

	tests := []struct {
		name           string
		token          string
		customReqJSON  string
		wantHTTPStatus int
		wantResJSON    string
	}{
		{
			name:           "Query Judy's wallets",
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkWTf-V867Uqqn68lo8zBju9-qNUgun47A",
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    fmt.Sprintf(successResFormat, 3),
		},
		{
			name:           "Query deleted account wallets",
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.GxjYTIKTqnSnjH9w7iMRRM4pUW00gcwKNxCqEc09z2I",
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.UnauthorizedToken),
		},
		{
			name:           "Query account with no token",
			token:          "",
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.UnauthorizedToken),
		},
		{
			name:           "Query don't exist token user",
			token:          "thistokendoesnotexist",
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.UnauthorizedToken),
		},
		{
			name:           "Query wallets from account without wallet",
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkEEW-V867Uqqn68lo8zBju9-qNUgun47A",
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.UnauthorizedToken),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var ctr Controller
			e := echo.New()
			req := httptest.NewRequest(http.MethodGet, "/getProfile", strings.NewReader(""))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", tt.token))
			rec := httptest.NewRecorder()
			c := e.NewContext(req, rec)
			err := ctr.GetUserWalletsByAuthToken(c)

			actuallyResult := rec.Body.String()
			reStr := regexp.MustCompile(`(Wallets.*}])`)
			actuallyResult = reStr.ReplaceAllString(actuallyResult, `Wallets":[]`)

			if assert.NoError(t, err) {
				assert.Equal(t, tt.wantHTTPStatus, rec.Code)
				assert.Equal(t, tt.wantResJSON, actuallyResult)
			}
		})
	}
}
