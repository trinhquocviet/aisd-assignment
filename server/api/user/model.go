package user

import (
	api "bitbucket.org/trinhquocviet/aisd-assignment/server/api"
	model "bitbucket.org/trinhquocviet/aisd-assignment/server/model"
	"time"
)

type (
	// IGetProfileRes the interface for GetProfileRes
	IGetProfileRes interface {
		Mapping(model.User)
	}

	// profileRes the Struct for profile information
	profileRes struct {
		ID          int64     `json:"UserID"`
		DisplayName string    `json:"Name"`
		Email       string    `json:"Email"`
		CreatedAt   time.Time `json:"CreatedAt"`
	}

	// GetProfileRes the Struct for binding data to response
	GetProfileRes struct {
		api.DefaultSuccessRes
		Profile profileRes `json:"Profile"`
	}
)

// Mapping the function to map data from dbo to response
func (g *GetProfileRes) Mapping(user model.User) {
	g.Success = true
	g.Profile = profileRes{
		ID:          user.ID,
		DisplayName: user.DisplayName,
		Email:       user.Email,
		CreatedAt:   user.CreatedAt,
	}
}

type (
	// walletRes the Struct for profile information
	walletRes struct {
		ID         int64      `json:"WalletID"`
		CurrencyID uint       `json:"CurrencyID"`
		Balance    string     `json:"Amount"`
		UpdatedAt  *time.Time `json:"UpdatedAt"`
	}

	// GetWalletsRes the Struct for binding data to response
	GetWalletsRes struct {
		api.DefaultSuccessRes
		Wallets []walletRes `json:"Wallets"`
		Count   int         `json:"Count"`
	}

	// IGetWalletsRes the interface for GetWalletsRes
	IGetWalletsRes interface {
		Mapping([]model.Wallet)
	}
)

// Mapping the function to map data from dbo to response
func (g *GetWalletsRes) Mapping(wallets []model.Wallet) {
	g.Success = true
	if len(wallets) > 0 {
		for _, wallet := range wallets {
			g.Wallets = append(g.Wallets, walletRes{
				ID:         wallet.ID,
				CurrencyID: wallet.CurrencyID,
				Balance:    wallet.Balance.StringFixed(2),
				UpdatedAt:  wallet.UpdatedAt,
			})
		}
	} else {
		g.Wallets = []walletRes{}
	}
	g.Count = len(wallets)
}
