package user

import (
	api "bitbucket.org/trinhquocviet/aisd-assignment/server/api"
	msg "bitbucket.org/trinhquocviet/aisd-assignment/server/internal/message"
	model "bitbucket.org/trinhquocviet/aisd-assignment/server/model"
	"fmt"
	logrus "github.com/Sirupsen/logrus"
)

// Repository use to interact with database
type Repository struct {
	api.RepositoryBase
}

// GetUserByAuthToken get user information by authToken
func (r *Repository) GetUserByAuthToken(token string) (user model.User, err error) {
	r.OpenConnection()
	defer r.DB.Close()

	user, err = r.QueryUserByAuthToken(token)

	return
}

// GetUserWalletsByAuthToken get user's wallets by authToken
func (r *Repository) GetUserWalletsByAuthToken(token string) (wallets []model.Wallet, err error) {
	r.OpenConnection()
	defer r.DB.Close()

	user, err := r.QueryUserByAuthToken(token)
	if err != nil || user.ID <= 0 {
		wallets = []model.Wallet{}
		return
	}

	rows, err := r.DB.Raw("CALL GetUserWalletsByAuthToken(?)", token).Rows()
	defer rows.Close()
	if err != nil {
		logrus.Error("GetUserWalletsByAuthToken: ", err)
		wallets = []model.Wallet{}
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	for rows.Next() {
		wallet := model.Wallet{}
		err = rows.Scan(
			&wallet.ID,
			&wallet.UID,
			&wallet.CurrencyID,
			&wallet.Balance,
			&wallet.CreatedAt,
			&wallet.UpdatedAt,
		)

		if err != nil {
			logrus.Error("GetUserWalletsByAuthToken: ", err)
			wallets = []model.Wallet{}
			return
		}

		wallets = append(wallets, wallet)
	}

	if err = rows.Err(); err != nil {
		logrus.Error("GetUserWalletsByAuthToken: ", err)
		wallets = []model.Wallet{}
	}
	return
}
