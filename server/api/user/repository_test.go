package user

import (
	"reflect"
	"testing"

	// api "bitbucket.org/trinhquocviet/aisd-assignment/server/api"
	model "bitbucket.org/trinhquocviet/aisd-assignment/server/model"
)

func TestRepository_GetUserByAuthToken(t *testing.T) {
	funcName := "GetUserByAuthToken()"

	tests := []struct {
		name      string
		token     string
		wantUser  model.User
		wantError bool
	}{
		{
			name:      "Query Judy info",
			token:     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkWTf-V867Uqqn68lo8zBju9-qNUgun47A",
			wantUser:  model.User{Email: "judy@example.com"},
			wantError: false,
		},
		{
			name:      "Query deleted user",
			token:     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.GxjYTIKTqnSnjH9w7iMRRM4pUW00gcwKNxCqEc09z2I",
			wantUser:  model.User{},
			wantError: true,
		},
		{
			name:      "Query no token user",
			token:     "",
			wantUser:  model.User{},
			wantError: true,
		},
		{
			name:      "Query don't exist token user",
			token:     "thistokendoesnotexist",
			wantUser:  model.User{},
			wantError: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := Repository{}
			r.OpenConnection()
			defer r.DB.Close()
			rawUser, err := r.QueryUserByAuthToken(tt.token)
			user := model.User{Email: rawUser.Email}

			if !tt.wantError {
				if err != nil {
					t.Errorf("%s gotErr = %v, want %v", funcName, err.Error(), "nil")
				} else if !reflect.DeepEqual(user, tt.wantUser) {
					t.Errorf("%s incompatible", funcName)
				}
			} else {
				if err == nil {
					t.Errorf("%s gotErr = %v, want %v", funcName, err, tt.wantError)
				}
			}
		})
	}
}

func TestRepository_GetUserWalletsByAuthToken(t *testing.T) {
	funcName := "GetUserWalletsByAuthToken()"

	tests := []struct {
		name        string
		token       string
		wantWallets []model.Wallet
		wantError   bool
	}{
		{
			name:        "Query Judy wallets",
			token:       "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkWTf-V867Uqqn68lo8zBju9-qNUgun47A",
			wantWallets: []model.Wallet{model.Wallet{ID: 1}, model.Wallet{ID: 2}, model.Wallet{ID: 3}},
			wantError:   false,
		},
		{
			name:        "Query wallets from deleted account",
			token:       "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.GxjYTIKTqnSnjH9w7iMRRM4pUW00gcwKNxCqEc09z2I",
			wantWallets: []model.Wallet{},
			wantError:   true,
		},
		{
			name:        "Query wallets from account without token",
			token:       "",
			wantWallets: []model.Wallet{},
			wantError:   true,
		},
		{
			name:        "Query wallets from account without wallet",
			token:       "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkEEW-V867Uqqn68lo8zBju9-qNUgun47A",
			wantWallets: []model.Wallet{},
			wantError:   true,
		},
		{
			name:        "Query wallets from non user",
			token:       "thisisnottoken",
			wantWallets: []model.Wallet{},
			wantError:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := Repository{}
			r.OpenConnection()
			defer r.DB.Close()
			rawWallets, err := r.GetUserWalletsByAuthToken(tt.token)
			wallets := []model.Wallet{}

			for _, w := range rawWallets {
				wallets = append(wallets, model.Wallet{ID: w.ID})
			}

			if !tt.wantError {
				if err != nil {
					t.Errorf("%s gotErr = %v, want %v", funcName, err.Error(), tt.wantError)
				} else if len(wallets) != len(tt.wantWallets) || !reflect.DeepEqual(wallets, tt.wantWallets) {
					t.Errorf("%s incompatible", funcName)
				}
			} else {
				if err == nil {
					t.Errorf("%s gotErr = %v, want %v", funcName, err, tt.wantError)
				}
			}
		})
	}
}
