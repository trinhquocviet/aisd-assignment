package user

import (
	api "bitbucket.org/trinhquocviet/aisd-assignment/server/api"
	internal "bitbucket.org/trinhquocviet/aisd-assignment/server/internal"
	"github.com/labstack/echo"
)

// Controller the handler function extend from api/controller_base.go
type Controller struct {
	api.ControllerBase
	repository Repository
	validator  internal.Validator
}

// GetProfile will return response with profile detail
func (ctr *Controller) GetProfile(c echo.Context) error {
	// Get Token
	token, err := ctr.GetRequestToken(c)
	if err != nil {
		return ctr.ErrorRes(c, err)
	}

	// Execute
	user, err := ctr.repository.GetUserByAuthToken(token)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Mapping
	var res GetProfileRes
	res.Mapping(user)

	return ctr.SuccessRes(c, res)
}

// GetUserWalletsByAuthToken will return response with wallets's user
func (ctr *Controller) GetUserWalletsByAuthToken(c echo.Context) error {
	// Get Token
	token, err := ctr.GetRequestToken(c)
	if err != nil {
		return ctr.ErrorRes(c, err)
	}

	// Execute
	wallets, err := ctr.repository.GetUserWalletsByAuthToken(token)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Mapping
	var res GetWalletsRes
	res.Mapping(wallets)

	return ctr.SuccessRes(c, res)
}
