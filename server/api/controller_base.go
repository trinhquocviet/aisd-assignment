package api

import (
	msg "bitbucket.org/trinhquocviet/aisd-assignment/server/internal/message"
	"fmt"
	"github.com/labstack/echo"
	"net/http"
	"reflect"
	"sort"
	"strings"
)

type (
	// DefaultSuccessRes the default response
	DefaultSuccessRes struct {
		Success bool `json:"Success"`
	}

	// DefaultErrorRes the default response
	DefaultErrorRes struct {
		Success bool   `json:"Success"`
		Error   string `json:"Error"`
	}

	// ControllerBase the base class for api API class
	ControllerBase struct {
		privateRoutes []string
	}
)

// ValidateRoute validate route has Authorization
func (ctr *ControllerBase) ValidateRoute(e echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if len(ctr.privateRoutes) > 0 {
			idx := sort.SearchStrings(ctr.privateRoutes, c.Request().RequestURI)
			if idx >= 0 {
				authorization := ctr.getHeaderValue(c, "Authorization")
				if len(authorization) <= 0 {
					return ctr.ErrorRes(c, msg.MissingToken)
				}
			}
		}

		return e(c)
	}
}

// RestrictUSOnly validate country header is US
func (ctr *ControllerBase) RestrictUSOnly(e echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		country := ctr.getHeaderValue(c, "country")
		if country != "US" {
			return ctr.ErrorRes(c, msg.UnauthorizedAPI)
		}
		return e(c)
	}
}

// UpdatePrivateRoutes update list private routes
func (ctr *ControllerBase) UpdatePrivateRoutes(routes []string) {
	ctr.privateRoutes = routes
}

// GetRequestToken get token from header
func (ctr *ControllerBase) GetRequestToken(c echo.Context) (token string, err error) {
	token = ""
	err = nil

	authToken := ctr.getHeaderValue(c, "Authorization")
	if len(authToken) <= 0 {
		err = fmt.Errorf(msg.MissingToken)
		return
	}

	token = strings.Replace(authToken, "Bearer ", "", -1)
	return
}

func (ctr *ControllerBase) getHeaderValue(c echo.Context, key string) string {
	return c.Request().Header.Get(key)
}

func (ctr *ControllerBase) parse(args []interface{}) (code int, res interface{}) {
	if len(args) > 0 {
		count := len(args)
		if count == 1 {
			args0 := reflect.TypeOf(args[0]).String()
			if args0 == "int" {
				code = args[0].(int)
			} else if args0 == "string" || args0 == "errors.errorString" {
				res = DefaultErrorRes{Success: false, Error: args[0].(string)}
			} else if reflect.TypeOf(args[0]) != nil {
				res = args[0]
			}
		} else if count == 2 {
			args0 := reflect.TypeOf(args[0]).String()
			args1 := reflect.TypeOf(args[1]).String()
			if args0 == "int" {
				code = args[0].(int)
			}
			if args1 == "string" || args1 == "*errors.errorString" {
				res = DefaultErrorRes{Success: false, Error: args[0].(string)}
			} else if reflect.TypeOf(args[1]) != nil {
				res = args[1]
			}
		}
	}

	return
}

// SuccessRes return the json response
func (ctr *ControllerBase) SuccessRes(c echo.Context, args ...interface{}) error {
	code, res := ctr.parse(args)

	if code < 200 {
		code = http.StatusOK
	}
	if res == nil {
		res = DefaultSuccessRes{
			Success: true,
		}
	}

	return c.JSON(code, res)
}

// ErrorRes return the json response
func (ctr *ControllerBase) ErrorRes(c echo.Context, args ...interface{}) error {
	code, res := ctr.parse(args)

	if code < 200 {
		code = http.StatusBadRequest
	}
	if res == nil {
		res = DefaultErrorRes{
			Success: false,
			Error:   msg.DefaultError,
		}
	}

	return c.JSON(code, res)
}
