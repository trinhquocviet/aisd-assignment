package transaction

import (
	api "bitbucket.org/trinhquocviet/aisd-assignment/server/api"
	internal "bitbucket.org/trinhquocviet/aisd-assignment/server/internal"
	"github.com/labstack/echo"
)

// Controller the handler function extend from api/controller_base.go
type Controller struct {
	api.ControllerBase
	repository Repository
	validator  internal.Validator
}

// TopUp will return response all available currencies
func (ctr *Controller) TopUp(c echo.Context) error {
	// Build query
	var req TopUpReq
	if err := c.Bind(&req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Validate
	if err := ctr.validator.Validate(req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// get token
	token, err := ctr.GetRequestToken(c)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Query
	tx, err := ctr.repository.TopUp(token, req)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Mapping
	var res TopUpRes
	res.Mapping(tx)

	return ctr.SuccessRes(c, res)
}

// SendMoney will do action exchange money form
func (ctr *Controller) SendMoney(c echo.Context) error {
	// Build query
	var req SendMoneyReq
	if err := c.Bind(&req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Validate
	if err := ctr.validator.Validate(req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// get token
	token, err := ctr.GetRequestToken(c)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Query
	txs, err := ctr.repository.SendMoney(token, req)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Mapping
	var res SendMoneyRes
	res.Mapping(txs)

	return ctr.SuccessRes(c, res)
}
