package transaction

import (
	api "bitbucket.org/trinhquocviet/aisd-assignment/server/api"
	constants "bitbucket.org/trinhquocviet/aisd-assignment/server/constants"
	helper "bitbucket.org/trinhquocviet/aisd-assignment/server/internal/helper"
	msg "bitbucket.org/trinhquocviet/aisd-assignment/server/internal/message"
	model "bitbucket.org/trinhquocviet/aisd-assignment/server/model"
	"fmt"
	logrus "github.com/Sirupsen/logrus"
	"github.com/shopspring/decimal"
)

// Repository use to interact with database
type Repository struct {
	api.RepositoryBase
}

// QueryWalletByUserAndCurrency the function return user info based on auth token
func (r *Repository) QueryWalletByUserAndCurrency(userID int64, currencyID uint) (wallet model.Wallet, err error) {
	err = r.DB.Table("wallets").Select("id").Where(
		"uid = ? and currency_id = ?", userID, currencyID,
	).First(&wallet).Error

	if err != nil {
		logrus.Error("QueryWalletByUserAndCurrency: ", err)
		wallet = model.Wallet{}
		err = fmt.Errorf(msg.DefaultError)
	}

	return
}

// ExecCreateTransaction the function call store proceduce to insert
func (r *Repository) ExecCreateTransaction(hashKey string, tx model.Transaction) (model.Transaction, error) {
	// get user info
	err := r.DB.Exec(
		"CALL CreateTransaction (?, ?, ?, ?, ?, ?, ?, ?)",
		hashKey, tx.WalletID, tx.TX, tx.Amount.StringFixed(2), tx.Action, tx.Description, tx.Receipt, tx.Status,
	).Error

	if err != nil {
		logrus.Error("ExecCreateTransaction: ", err)
		return model.Transaction{}, err
	}

	err = r.DB.Select("id, created_at").Table("transactions").Where(
		"tx = ?", tx.TX,
	).Row().Scan(&tx.ID, &tx.CreatedAt)

	if err != nil {
		logrus.Error("ExecCreateTransaction: ", err)
		return model.Transaction{}, err
	}

	return tx, nil
}

// TopUp create the transaction
func (r *Repository) TopUp(token string, req TopUpReq) (tx model.Transaction, err error) {
	r.OpenConnection()
	defer r.DB.Close()
	tx = model.Transaction{}

	user, err := r.QueryUserByAuthToken(token)
	if err != nil {
		logrus.Error("TopUp: ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	wallet, err := r.QueryWalletByUserAndCurrency(user.ID, req.CurrencyID)
	if err != nil {
		logrus.Error("TopUp: ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	amount, err := decimal.NewFromString(req.Amount)
	if err != nil {
		logrus.Error("TopUp: ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	tx = model.Transaction{
		WalletID:    wallet.ID,
		TX:          helper.GenerateTransactionID(constants.TransactionPrefix["TOPUP"]),
		Amount:      amount,
		Action:      constants.TransactionAction["IN"],
		Description: req.Description,
		Receipt:     req.Receipt,
		Status:      constants.TransactionStatus["SUCCESS"],
	}

	tx, err = r.ExecCreateTransaction(user.HashKey, tx)
	return
}

// ExecExchangeMoney do exchange money
func (r *Repository) ExecExchangeMoney(hashKey string, req SendMoneyReq) (txs []model.Transaction, err error) {
	txID := helper.GenerateTransactionID(constants.TransactionPrefix["EXCHANGE"])

	amount, err := decimal.NewFromString(req.Amount)
	err = r.DB.Exec(
		"CALL ExchangeMoney (?, ?, ?, ?, ?)",
		hashKey, req.FromCurrencyID, req.ToCurrencyID, txID, amount,
	).Error

	if err != nil {
		logrus.Error("ExecExchangeMoney: ", err)
		return
	}

	err = r.DB.Select("id, tx, status, created_at").Table("transactions").Where(
		"tx LIKE ?", (txID + "%"),
	).Find(&txs).Error

	if err != nil {
		logrus.Error("ExecExchangeMoney: ", err)
		return
	}

	return
}

// SendMoney do exchange money
func (r *Repository) SendMoney(token string, req SendMoneyReq) (txs []model.Transaction, err error) {
	r.OpenConnection()
	defer r.DB.Close()
	txs = []model.Transaction{}

	user, err := r.QueryUserByAuthToken(token)
	if err != nil {
		logrus.Error("SendMoney: ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	txs, err = r.ExecExchangeMoney(user.HashKey, req)
	return
}
