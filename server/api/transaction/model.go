package transaction

import (
	api "bitbucket.org/trinhquocviet/aisd-assignment/server/api"
	model "bitbucket.org/trinhquocviet/aisd-assignment/server/model"
	"time"
)

type (
	// TopUpReq the struct to bind data to request
	TopUpReq struct {
		CurrencyID  uint   `json:"currencyID" validate:"required"`
		Amount      string `json:"amount" validate:"required"`
		Description string `json:"description" validate:"max=200"`
		Receipt     string `json:"receipt" validate:"max=100"`
	}

	transactionRes struct {
		ID        int64     `json:"TopUpID"`
		TX        string    `json:"TX"`
		Status    string    `json:"Status"`
		CreatedAt time.Time `json:"CreatedAt"`
	}

	// TopUpRes the struct to bind data to response
	TopUpRes struct {
		api.DefaultSuccessRes
		Transaction transactionRes `json:"Transaction"`
	}

	// ITopUpRes the interface for TopUpRes
	ITopUpRes interface {
		Mapping(model.Transaction)
	}
)

// Mapping the function to map data from dbo to response
func (t *TopUpRes) Mapping(transaction model.Transaction) {
	t.Success = true
	t.Transaction = transactionRes{
		ID:        transaction.ID,
		TX:        transaction.TX,
		Status:    transaction.Status,
		CreatedAt: transaction.CreatedAt,
	}
}

type (
	// SendMoneyReq the struct to bind data to request
	SendMoneyReq struct {
		FromCurrencyID uint   `json:"fromCurrencyID" validate:"required"`
		ToCurrencyID   uint   `json:"toCurrencyID" validate:"required"`
		Type           string `json:"type" validate:"required"`
		Amount         string `json:"amount" validate:"required"`
	}

	transactionSendMoneyRes struct {
		ID        int64     `json:"ID"`
		TX        string    `json:"TX"`
		Status    string    `json:"Status"`
		CreatedAt time.Time `json:"CreatedAt"`
	}

	// SendMoneyRes the struct to bind data to response
	SendMoneyRes struct {
		api.DefaultSuccessRes
		Transactions []transactionSendMoneyRes `json:"Transactions"`
	}

	// ISendMoneyRes the interface for SendMoneyRes
	ISendMoneyRes interface {
		Mapping([]model.Transaction)
	}
)

// Mapping the function to map data from dbo to response
func (t *SendMoneyRes) Mapping(txs []model.Transaction) {
	t.Success = true
	if len(txs) > 0 {
		for _, tx := range txs {
			txRes := transactionSendMoneyRes{
				ID:        tx.ID,
				TX:        tx.TX,
				Status:    tx.Status,
				CreatedAt: tx.CreatedAt,
			}

			t.Transactions = append(t.Transactions, txRes)
		}

	} else {
		t.Transactions = []transactionSendMoneyRes{}
	}
}
