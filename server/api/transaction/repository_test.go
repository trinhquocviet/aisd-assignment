package transaction

import (
	"testing"

	model "bitbucket.org/trinhquocviet/aisd-assignment/server/model"
)

func TestRepository_QueryWalletByUserAndCurrency(t *testing.T) {
	funcName := "QueryWalletByUserAndCurrency()"

	type args struct {
		userID     int64
		currencyID uint
	}
	tests := []struct {
		name       string
		args       args
		wantWallet model.Wallet
		wantErr    bool
	}{
		{
			name:       "Query Judy's SGD wallet",
			args:       args{userID: 1, currencyID: 1},
			wantWallet: model.Wallet{ID: 1},
			wantErr:    false,
		},
		{
			name:       "Query Judy's USD wallet",
			args:       args{userID: 1, currencyID: 2},
			wantWallet: model.Wallet{ID: 2},
			wantErr:    false,
		},
		{
			name:       "Query Judy's MYR wallet",
			args:       args{userID: 1, currencyID: 3},
			wantWallet: model.Wallet{ID: 3},
			wantErr:    false,
		},
		{
			name:       "Query Judy's nonexist wallet",
			args:       args{userID: 1, currencyID: 4},
			wantWallet: model.Wallet{ID: 0},
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var r Repository
			r.OpenConnection()
			defer r.DB.Close()

			gotWallet, err := r.QueryWalletByUserAndCurrency(tt.args.userID, tt.args.currencyID)

			if err != nil && !tt.wantErr {
				t.Errorf("%s error = %v, wantErr %v", funcName, err, tt.wantErr)
				return
			}

			if gotWallet.ID != tt.wantWallet.ID {
				t.Errorf("%s = %v, want %v", funcName, gotWallet, tt.wantWallet)
			}
		})
	}
}

func TestRepository_TopUp(t *testing.T) {
	type args struct {
		token string
		req   TopUpReq
	}
	tests := []struct {
		name    string
		args    args
		wantTx  model.Transaction
		wantErr bool
	}{
		{
			name:    "Top up to Judy SGD",
			args:    args{
									token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkWTf-V867Uqqn68lo8zBju9-qNUgun47A",
									req: TopUpReq{CurrencyID: 1, Amount: "1000", Description: "", Receipt: ""},
								},
			wantTx:  model.Transaction{Status: "success"},
			wantErr: false,
		},
		{
			name:    "Top up to Judy nonexist wallet",
			args:    args{
									token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkWTf-V867Uqqn68lo8zBju9-qNUgun47A",
									req: TopUpReq{CurrencyID: 4, Amount: "1000", Description: "", Receipt: ""},
								},
			wantTx:  model.Transaction{},
			wantErr: true,
		},
		{
			name:    "Top up to deleted account",
			args:    args{
									token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.GxjYTIKTqnSnjH9w7iMRRM4pUW00gcwKNxCqEc09z2I",
									req: TopUpReq{CurrencyID: 1, Amount: "1000", Description: "", Receipt: ""},
								},
			wantTx:  model.Transaction{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var r Repository
			r.OpenConnection()
			defer r.DB.Close()

			gotTx, err := r.TopUp(tt.args.token, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("TopUp() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotTx.Status != tt.wantTx.Status {
				t.Errorf("TopUp() = %v, want %v", gotTx, tt.wantTx)
			}
		})
	}
}
