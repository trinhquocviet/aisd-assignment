package currency

import (
	api "bitbucket.org/trinhquocviet/aisd-assignment/server/api"
	msg "bitbucket.org/trinhquocviet/aisd-assignment/server/internal/message"
	model "bitbucket.org/trinhquocviet/aisd-assignment/server/model"
	"fmt"
	logrus "github.com/Sirupsen/logrus"
)

// Repository use to interact with database
type Repository struct {
	api.RepositoryBase
}

// GetAllCurrencies get all currencies
func (r *Repository) GetAllCurrencies() (currencies []model.Currency, err error) {
	r.OpenConnection()
	defer r.DB.Close()

	err = r.DB.Table("currencies").Find(&currencies).Error
	if err != nil {
		logrus.Error("GetAllCurrencies: ", err)
		currencies = []model.Currency{}
		err = fmt.Errorf(msg.DefaultError)
	}

	return
}

// GetExchangeRate get exchange rate by from and to currencry
func (r *Repository) GetExchangeRate(fromCurrencyID uint, toCurrencyID uint) (exchangeRate model.ExchangeRate, err error) {
	r.OpenConnection()
	defer r.DB.Close()

	err = r.DB.Where(&model.ExchangeRate{
		FromID: fromCurrencyID,
		ToID:   toCurrencyID,
	}).First(&exchangeRate).Error

	if err != nil {
		exchangeRate = model.ExchangeRate{}
		err = fmt.Errorf(msg.CanNotGetExchangeRate)
		return
	}

	return
}
