package currency

import (
	api "bitbucket.org/trinhquocviet/aisd-assignment/server/api"
	model "bitbucket.org/trinhquocviet/aisd-assignment/server/model"
	"github.com/shopspring/decimal"
)

type (
	// IGetLookUpValuesRes the interface for GetLookUpValuesRes
	IGetLookUpValuesRes interface {
		Mapping(model.User)
	}

	currencyRes struct {
		ID   uint   `json:"CurrencyID"`
		Code string `json:"Name"`
	}

	// GetLookUpValuesRes the Struct for binding data to response
	GetLookUpValuesRes struct {
		api.DefaultSuccessRes
		Currencies []currencyRes `json:"Currencies"`
		Count      int           `json:"Count"`
	}
)

// Mapping the function to map data from dbo to response
func (g *GetLookUpValuesRes) Mapping(currencies []model.Currency) {
	g.Success = true
	if len(currencies) > 0 {
		for _, currency := range currencies {
			g.Currencies = append(g.Currencies, currencyRes{
				ID:   currency.ID,
				Code: currency.Code,
			})
		}
	} else {
		g.Currencies = []currencyRes{}
	}
	g.Count = len(currencies)
}

type (
	// CalculateRateReq the Struct for binding data from request
	CalculateRateReq struct {
		FromCurrencyID uint   `json:"fromCurrencyID" validate:"required"`
		ToCurrencyID   uint   `json:"toCurrencyID" validate:"required"`
		Type           string `json:"type" validate:"required"`
		Amount         string `json:"amount" validate:"required"`
	}

	calculateDataRes struct {
		Rate   string `json:"rate"`
		Amount string `json:"amount"`
	}

	// CalculateRateRes the Struct for binding data from request
	CalculateRateRes struct {
		api.DefaultSuccessRes
		Data calculateDataRes `json:"data"`
	}
)

type (
	// GetRateReq the Struct for binding data from request
	GetRateReq struct {
		FromCurrencyID uint `json:"fromCurrencyID" validate:"required"`
		ToCurrencyID   uint `json:"toCurrencyID" validate:"required"`
	}

	// IGetRateRes the interface for GetRateRes
	IGetRateRes interface {
		Mapping(model.User)
	}

	// GetRateRes the Struct for binding data from request
	GetRateRes struct {
		api.DefaultSuccessRes
		Rate string `json:"rate"`
	}
)

// Mapping the function to map data from dbo to response
func (g *GetRateRes) Mapping(exchangeRate model.ExchangeRate) {
	g.Success = true
	g.Rate = decimal.NewFromFloat32(exchangeRate.Rate).StringFixed(2)
}
