package currency

import (
	api "bitbucket.org/trinhquocviet/aisd-assignment/server/api"
	internal "bitbucket.org/trinhquocviet/aisd-assignment/server/internal"
	"github.com/labstack/echo"
	"github.com/shopspring/decimal"
)

// Controller the handler function extend from api/controller_base.go
type Controller struct {
	api.ControllerBase
	repository Repository
	validator  internal.Validator
}

// GetAvailableCurrencies will return response all available currencies
func (ctr *Controller) GetAvailableCurrencies(c echo.Context) error {
	// Execute
	currencies, err := ctr.repository.GetAllCurrencies()
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Mapping
	var res GetLookUpValuesRes
	res.Mapping(currencies)

	return ctr.SuccessRes(c, res)
}

func (ctr *Controller) GetRate(c echo.Context) error {
	// Build query
	var req GetRateReq
	if err := c.Bind(&req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Validate
	if err := ctr.validator.Validate(req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Query
	exchangeRate, err := ctr.repository.GetExchangeRate(req.FromCurrencyID, req.ToCurrencyID)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Mapping
	var res GetRateRes
	res.Mapping(exchangeRate)

	return ctr.SuccessRes(c, res)
}

// CalculateRate will return the rate all available currencies
func (ctr *Controller) CalculateRate(c echo.Context) error {
	// Build query
	var req CalculateRateReq
	if err := c.Bind(&req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Validate
	if err := ctr.validator.Validate(req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Execute
	fromCurrencyID := req.FromCurrencyID
	toCurrencyID := req.ToCurrencyID
	// if req.Type == "TO" {
	// 	fromCurrencyID = req.ToCurrencyID
	// 	toCurrencyID = req.FromCurrencyID
	// }

	exchangeRate, err := ctr.repository.GetExchangeRate(fromCurrencyID, toCurrencyID)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Build response
	amount, err := decimal.NewFromString(req.Amount)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	var res CalculateRateRes

	res.Success = true
	res.Data = calculateDataRes{
		Rate: decimal.NewFromFloat32(exchangeRate.Rate).StringFixed(2),
		Amount: amount.Mul(
			decimal.NewFromFloat32(exchangeRate.Rate),
		).StringFixed(2),
	}

	return ctr.SuccessRes(c, res)
}
