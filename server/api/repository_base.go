package api

import (
	msg "bitbucket.org/trinhquocviet/aisd-assignment/server/internal/message"
	model "bitbucket.org/trinhquocviet/aisd-assignment/server/model"
	"fmt"
	logrus "github.com/Sirupsen/logrus"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"os"
)


// RepositoryBase the base class for api API class
type RepositoryBase struct {
	DB *gorm.DB
}



// OpenConnection the function to init the db connection and assign to Base.DB
func (r *RepositoryBase) OpenConnection() (err error) {
	cnStr := fmt.Sprintf(
		`%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local`,
		os.Getenv("MYSQL_USER"),
		os.Getenv("MYSQL_PASSWORD"),
		os.Getenv("MYSQL_HOST"),
		os.Getenv("MYSQL_PORT"),
		os.Getenv("MYSQL_DATABASE"),
	)
	fmt.Println()

	r.DB, err = gorm.Open("mysql", cnStr)
	return
}

// QueryUserByAuthToken the function return user info based on auth token
func (r *RepositoryBase) QueryUserByAuthToken(token string) (user model.User, err error) {
	// get user info
	err = r.DB.Table("users").Where("token = ? AND deleted_at IS NULL", token).First(&user).Error
	if err != nil || user.ID <= 0 {
		logrus.Error("QueryUserByAuthToken: ", err)
		err = fmt.Errorf(msg.UnauthorizedToken)
	}
	return
}
