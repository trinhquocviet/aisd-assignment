package api

import (
	"reflect"
	"testing"

	model "bitbucket.org/trinhquocviet/aisd-assignment/server/model"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func TestRepositoryBase_QueryUserByAuthToken(t *testing.T) {
	funcName := "RepositoryBase.QueryUserByAuthToken()"

	tests := []struct {
		name      string
		token     string
		wantUser  model.User
		wantError bool
	}{
		{
			name:      "Query Judy info",
			token:     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkWTf-V867Uqqn68lo8zBju9-qNUgun47A",
			wantUser:  model.User{Email: "judy@example.com"},
			wantError: false,
		},
		{
			name:      "Query deleted user",
			token:     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.GxjYTIKTqnSnjH9w7iMRRM4pUW00gcwKNxCqEc09z2I",
			wantUser:  model.User{},
			wantError: true,
		},
		{
			name:      "Query no token user",
			token:     "",
			wantUser:  model.User{},
			wantError: true,
		},
		{
			name:      "Query don't exist token user",
			token:     "thistokendoesnotexist",
			wantUser:  model.User{},
			wantError: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := RepositoryBase{}
			r.OpenConnection()
			defer r.DB.Close()
			rawUser, err := r.QueryUserByAuthToken(tt.token)
			user := model.User{Email: rawUser.Email}

			if !tt.wantError {
				if err != nil {
					t.Errorf("%s gotErr = %v, want %v", funcName, err.Error(), "nil")
				} else if !reflect.DeepEqual(user, tt.wantUser) {
					t.Errorf("%s incompatible", funcName)
				}
			} else {
				if err == nil {
					t.Errorf("%s gotErr = %v, want %v", funcName, err, tt.wantError)
				}
			}
		})
	}
}
