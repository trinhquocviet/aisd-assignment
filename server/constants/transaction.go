package constants

var TransactionStatus = map[string]string{
	"ACCEPTED": "accepted",
	"PENDING":  "pending",
	"SENT":     "sent",
	"SUCCESS":  "success",
	"FAILED":   "failed",
	"CANCELED": "canceled",
}

var TransactionAction = map[string]string{
	"IN":  "in",
	"OUT": "out",
}

var TransactionPrefix = map[string]string{
	"TOPUP":    "TOP",
	"TRANSFER": "TF",
	"EXCHANGE": "EX",
	"WITHDRAW": "WD",
}
