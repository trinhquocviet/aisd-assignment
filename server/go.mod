module bitbucket.org/trinhquocviet/aisd-assignment/server

go 1.13

require (
	github.com/Sirupsen/logrus v1.0.5
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/jinzhu/gorm v1.9.11
	github.com/labstack/echo v3.3.5+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/sandalwing/echo-logrusmiddleware v0.0.0-20161226034227-1d7700fcf2d3
	github.com/shopspring/decimal v0.0.0-20191009025716-f1972eb1d1f5
	github.com/stretchr/testify v1.4.0
	github.com/valyala/fasttemplate v1.1.0 // indirect
	golang.org/x/crypto v0.0.0-20191112222119-e1110fd1c708 // indirect
	golang.org/x/sys v0.0.0-20191112214154-59a1497f0cea // indirect
	gopkg.in/go-playground/validator.v9 v9.30.0
	gopkg.in/yaml.v2 v2.2.5 // indirect
)
