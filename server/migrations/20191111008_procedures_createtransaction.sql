-- +goose Up
-- +goose StatementBegin
CREATE PROCEDURE `CreateTransaction`(
  IN p_hash_key VARCHAR(16),
    IN p_wallet_id BIGINT,
    IN p_tx VARCHAR(16),
    IN p_amount DECIMAL(12,2),
    IN p_action VARCHAR(3),
    IN p_description VARCHAR(200),
    IN p_receipt VARCHAR(100),
    IN p_status VARCHAR(10)
)
BEGIN
    DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
	END;
    
    DECLARE EXIT HANDLER FOR SQLWARNING 
	BEGIN
		ROLLBACK;
	END;
    
    START TRANSACTION;

    SET block_encryption_mode = 'aes-256-ecb';
    SET @new_balance = 0;
        
    -- select current balance
		SELECT CAST(AES_DECRYPT(UNHEX(balance), p_hash_key) AS DECIMAL(12,2))
      INTO @cur_balance
      FROM wallets
      WHERE id = p_wallet_id;
        
    IF p_action = 'out' THEN
			SET @new_balance = @cur_balance - p_amount;
		ELSE
			SET @new_balance = @cur_balance + p_amount;
    END IF;
		
    IF @new_balance > 0 THEN
			-- insert into transaction
			INSERT INTO transactions (`wallet_id`, `tx`, `amount`, `action`, `description`, `receipt`, `status`)
			VALUES (p_wallet_id, p_tx, HEX(AES_ENCRYPT(p_amount, p_hash_key)), p_action, p_description, p_receipt, p_status);
			
			-- update current balance
			UPDATE wallets
			SET balance = HEX(AES_ENCRYPT(CAST(@new_balance  AS DECIMAL(12,2)), p_hash_key))
			WHERE id = p_wallet_id;
    END IF;

  COMMIT;
END;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP PROCEDURE `CreateTransaction`;
-- +goose StatementEnd
