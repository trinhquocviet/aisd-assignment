-- +goose Up
-- +goose StatementBegin
-- default password 123456
-- SET @password = 'BA3253876AED6BC22D4A6FF53D8406C6AD864195ED144AB5C87621B6C233B548BAEAE6956DF346EC8C17F5EA10F35EE3CBC514797ED7DDD3145464E2A0BAB413';
-- SET @user01_hash_key = '*G-KaPdSgVkYp3s6';
-- SET @user02_hash_key = 'A%C*F-JaNdRgUkXp';
-- SET @user03_hash_key = '7w!z%C&F)J@NcRfU';
-- SET @user04_hash_key = 'q3t6w9z$C&E)H@Mc';
-- SET @user05_hash_key = 'VkYp3s6v9y$B&E(H';
-- SET @user01_secret_key = 'hb9jSSIYhtWDAvN9B1S8Q2oIkpyWvFYnFJBgTM71MrHZES51lt9FaJjJcK9yNHtj';
-- SET @user02_secret_key = 'CaaYbJmIk94AGpowebG16ItFVNjHJ2Qn4W7jzqxdMJxeiKw1p2oKQv0NkbjsPdXg';
-- SET @user03_secret_key = 'WwFCEjjY8bbz3d1Tke545j9q6gw14Qi0us6KDSlIj3oLy4Z0uIAgun5WqAEuesDE';
-- SET @user04_secret_key = '8vP6Eb23pZGqkEIEYPopeddWgnApQkbKTLxX7dSYQciyUoYF2B0I7iuPJ9w4Ez94';
-- SET @user05_secret_key = 'iGp1gychiuDarUxjbFcyAWhEcq6ezBN7kfpdy95jPMabx6VY0sXh3nXJ3MywNriS';
-- SET @user01_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkWTf-V867Uqqn68lo8zBju9-qNUgun47A';
-- SET @user02_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.GxjYTIKTqnSnjH9w7iMRRM4pUW_SgcwKNxCqEc09z2I';
-- SET @user03_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.xMF1AGPkkWVk46DY32zrA1dV-zlbVowImIWgECfuO0I';
-- SET @user04_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.Pt4ycrMnIFKtCbEDzhSOGWe9kyxeXUJJ-VfFqwVqFwk';
-- SET @user05_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.8j17o9xYoTAx0cKDZ308Qwfhhpjuxo_eS2GjLCeRTvw';
INSERT INTO `users` (`email`, `password`, `token`, `secret_key`, `hash_key`, `display_name`) VALUES
('judy@example.com','BA3253876AED6BC22D4A6FF53D8406C6AD864195ED144AB5C87621B6C233B548BAEAE6956DF346EC8C17F5EA10F35EE3CBC514797ED7DDD3145464E2A0BAB413','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkWTf-V867Uqqn68lo8zBju9-qNUgun47A','hb9jSSIYhtWDAvN9B1S8Q2oIkpyWvFYnFJBgTM71MrHZES51lt9FaJjJcK9yNHtj','*G-KaPdSgVkYp3s6','Judy Kaszper'),
('rich@example.com','BA3253876AED6BC22D4A6FF53D8406C6AD864195ED144AB5C87621B6C233B548BAEAE6956DF346EC8C17F5EA10F35EE3CBC514797ED7DDD3145464E2A0BAB413','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.GxjYTIKTqnSnjH9w7iMRRM4pUW_SgcwKNxCqEc09z2I','CaaYbJmIk94AGpowebG16ItFVNjHJ2Qn4W7jzqxdMJxeiKw1p2oKQv0NkbjsPdXg','A%C*F-JaNdRgUkXp','Richard Bilborough');
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd