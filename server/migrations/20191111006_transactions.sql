-- +goose Up
-- +goose StatementBegin
CREATE TABLE `transactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wallet_id` bigint(20) NOT NULL,
  `tx` varchar(18) DEFAULT NULL COMMENT 'max 16',
  `amount` varchar(64) NOT NULL COMMENT 'AES Encryption, mode ECB, hex',
  `action` varchar(3) NOT NULL COMMENT ' in or out',
  `description` varchar(200) DEFAULT NULL,
  `receipt` varchar(100) DEFAULT NULL,
  `status` varchar(10) NOT NULL COMMENT 'accepted, pending, sent, success, failed, canceled',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tx` (`tx`),
  KEY `wallet_id` (`wallet_id`),
  CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`wallet_id`) REFERENCES `wallets` (`id`)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS `transactions`;
-- +goose StatementEnd
