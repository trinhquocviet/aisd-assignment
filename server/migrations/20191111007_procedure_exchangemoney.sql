-- +goose Up
-- +goose StatementBegin
CREATE PROCEDURE `ExchangeMoney`(
	IN p_hash_key VARCHAR(16),
    IN p_from_currency_id INT,
    IN p_to_currency_id INT,
    IN p_tx VARCHAR(16),
    IN p_amount DECIMAL(12,2)
)
BEGIN
    DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
	END;
    
    DECLARE EXIT HANDLER FOR SQLWARNING 
	BEGIN
		ROLLBACK;
	END;
    
    START TRANSACTION;
    SET block_encryption_mode = 'aes-256-ecb';
        -- get exchange rate 
		SELECT rate
        INTO @exchange_rate
		FROM exchange_rates
		WHERE from_id = p_from_currency_id AND to_id = p_to_currency_id
        LIMIT 1;
        
		-- calculate new amount
        SET @amount_out = p_amount;
        SET @amount_in = p_amount * @exchange_rate;
			
		-- select current balance
		SELECT id, CAST(AES_DECRYPT(UNHEX(balance), p_hash_key) AS DECIMAL(12,2))
		INTO @wallet_out, @cur_balance_out
		FROM wallets
		WHERE id = p_from_currency_id;

        SELECT id, CAST(AES_DECRYPT(UNHEX(balance), p_hash_key) AS DECIMAL(12,2))
		INTO @wallet_in, @cur_balance_in
		FROM wallets
		WHERE id = p_to_currency_id;
			
		-- Check
        SET @new_balance_out = @cur_balance_out - @amount_out;
        SET @new_balance_in = @cur_balance_in + @amount_in;
			
		IF @new_balance_out > 0 AND @new_balance_in > 0 THEN
				-- insert into transaction
				INSERT INTO transactions (`wallet_id`, `tx`, `amount`, `action`, `status`)
				VALUES (@wallet_out, CONCAT(p_tx, '-1'), HEX(AES_ENCRYPT(CAST(@amount_out  AS DECIMAL(12,2)), p_hash_key)), 'out', 'success'),
					(@wallet_in, CONCAT(p_tx, '-2'), HEX(AES_ENCRYPT(CAST(@amount_in  AS DECIMAL(12,2)), p_hash_key)), 'in', 'success')
                ;
                
				-- update current balance
				UPDATE wallets
				SET balance = HEX(AES_ENCRYPT(CAST(@new_balance_out  AS DECIMAL(12,2)), p_hash_key))
				WHERE id = p_from_currency_id;
                
                UPDATE wallets
				SET balance = HEX(AES_ENCRYPT(CAST(@new_balance_in  AS DECIMAL(12,2)), p_hash_key))
				WHERE id = p_to_currency_id;
		END IF;

  COMMIT;
END;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP PROCEDURE `ExchangeMoney`;
-- +goose StatementEnd
