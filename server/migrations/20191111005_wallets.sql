-- +goose Up
-- +goose StatementBegin
CREATE TABLE `wallets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `balance` varchar(64) NOT NULL COMMENT 'AES Encryption, mode ECB, hex',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_wallets_index` (`uid`,`currency_id`),
  KEY `currency_id` (`currency_id`),
  CONSTRAINT `wallets_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`id`),
  CONSTRAINT `wallets_ibfk_2` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS `wallets`;
-- +goose StatementEnd
