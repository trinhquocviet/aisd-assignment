package model

import (
	"time"
)

// User the data transfer object from database
type User struct {
	ID          int64      `gorm:"column:id"`
	Email       string     `gorm:"column:email"`
	DisplayName string     `gorm:"column:display_name"`
	Token       string     `gorm:"column:token"`
	SecretKey   string     `gorm:"column:secret_key"`
	HashKey     string     `gorm:"column:hash_key"`
	CreatedAt   time.Time  `gorm:"column:created_at"`
	UpdatedAt   *time.Time `gorm:"column:updated_at"`
	DeletedAt   *time.Time `gorm:"column:deleted_at"`
}
