package model

import (
	"time"
)

// Currency the data transfer object from database
type Currency struct {
	ID        uint       `gorm:"column:id"`
	Code      string     `gorm:"column:code"`
	CreatedAt time.Time  `gorm:"column:created_at"`
	UpdatedAt *time.Time `gorm:"column:updated_at"`
	DeletedAt *time.Time `gorm:"column:deleted_at"`
}

// TableName define name for gorm Table
func (c *Currency) TableName() string {
	return "currencies"
}
