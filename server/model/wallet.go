package model

import (
	"github.com/shopspring/decimal"
	"time"
)

// Wallet the data transfer object from database
type Wallet struct {
	ID         int64           `gorm:"column:id"`
	UID        int64           `gorm:"column:uid"`
	CurrencyID uint            `gorm:"column:currency_id"`
	Balance    decimal.Decimal `gorm:"column:balance"`
	CreatedAt  time.Time       `gorm:"column:created_at"`
	UpdatedAt  *time.Time      `gorm:"column:updated_at"`
	DeletedAt  *time.Time      `gorm:"column:deleted_at"`
}
