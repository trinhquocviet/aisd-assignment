package model

// ExchangeRate the data transfer object from database
type ExchangeRate struct {
	ID     uint    `gorm:"column:id"`
	FromID uint    `gorm:"column:from_id"`
	ToID   uint    `gorm:"column:to_id"`
	Rate   float32 `gorm:"column:rate"`
}

// TableName define name for gorm Table
func (e *ExchangeRate) TableName() string {
	return "exchange_rates"
}
