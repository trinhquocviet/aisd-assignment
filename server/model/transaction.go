package model

import (
	"github.com/shopspring/decimal"
	"time"
)

// Transaction the data transfer object from database
type Transaction struct {
	ID          int64           `gorm:"column:id"`
	WalletID    int64           `gorm:"column:wallet_id"`
	TX          string          `gorm:"column:tx"`
	Amount      decimal.Decimal `gorm:"column:amount"`
	Action      string          `gorm:"column:action"`
	Description string          `gorm:"column:description"`
	Receipt     string          `gorm:"column:receipt"`
	Status      string          `gorm:"column:status"`
	CreatedAt   time.Time       `gorm:"column:created_at"`
	UpdatedAt   *time.Time      `gorm:"column:updated_at"`
}
