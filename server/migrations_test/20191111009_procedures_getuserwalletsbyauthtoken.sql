-- +goose Up
-- +goose StatementBegin
CREATE  PROCEDURE `GetUserWalletsByAuthToken`( IN p_token VARCHAR(255) )
BEGIN
  
  SET block_encryption_mode = 'aes-256-ecb';

	SELECT hash_key, id
	INTO @hash_key, @user_id
	FROM users
	WHERE token = p_token;

	SELECT 
    id,
    uid,
    currency_id,
    CAST(AES_DECRYPT(UNHEX(balance), @hash_key) AS DECIMAL (12 , 2 )) AS `balance`,
    created_at,
    updated_at
	FROM
		wallets
	WHERE
		uid = @user_id;
END;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP PROCEDURE `GetUserWalletsByAuthToken`;
-- +goose StatementEnd
