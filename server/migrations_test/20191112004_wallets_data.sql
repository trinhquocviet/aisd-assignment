-- +goose Up
-- +goose StatementBegin
-- default password 123456
-- SET @user01_hash_key = '*G-KaPdSgVkYp3s6';
-- SET @user02_hash_key = 'A%C*F-JaNdRgUkXp';
-- SET @user03_hash_key = '7w!z%C&F)J@NcRfU';
-- SET @user04_hash_key = 'q3t6w9z$C&E)H@Mc';
-- SET @user05_hash_key = 'VkYp3s6v9y$B&E(H';

-- CAST(AES_DECRYPT() AS DECIMAL(12,2))
INSERT INTO wallets (`uid`, `currency_id`, `balance`) VALUES
  (1, 1, '5875F0C9259629FCF8ADABD69EFA2E64'),
  (1, 2, '5875F0C9259629FCF8ADABD69EFA2E64'),
  (1, 3, '5875F0C9259629FCF8ADABD69EFA2E64'),
  (2, 1, '0EDD3380286E5317BA6632BB9AB3CE06'),
  (2, 2, '0EDD3380286E5317BA6632BB9AB3CE06'),
  (2, 3, '0EDD3380286E5317BA6632BB9AB3CE06');
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd