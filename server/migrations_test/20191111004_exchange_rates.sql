-- +goose Up
-- +goose StatementBegin
CREATE TABLE `exchange_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `rate` float NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_exchange_rates` (`from_id`,`to_id`),
  KEY `to_id` (`to_id`),
  CONSTRAINT `exchange_rates_ibfk_1` FOREIGN KEY (`from_id`) REFERENCES `currencies` (`id`),
  CONSTRAINT `exchange_rates_ibfk_2` FOREIGN KEY (`to_id`) REFERENCES `currencies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS `exchange_rates`;
-- +goose StatementEnd
