-- +goose Up
-- +goose StatementBegin
INSERT INTO `exchange_rates` (`from_id`, `to_id`, `rate`) VALUES
  (1, 2, 0.73), -- sgd to usd
  (2, 1, 1.36), -- usd to sgd
  (1, 3, 3.05), -- sgd to myr
  (3, 1, 0.33), -- myr to sgd
  (2, 3, 4.14), -- usd to myr
  (3, 2, 0.24); -- myr to usd
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd