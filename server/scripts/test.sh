#!/bin/sh
# Change directory to server
cd $GOPATH/src/bitbucket.org/trinhquocviet/aisd-assignment/server

# export enviroment
export $(grep -v '^#' .env | xargs) && \
export CN_STRING="root:aisd_pass_root@(${MYSQL_HOST}:${MYSQL_PORT})/${MYSQL_DATABASE_TEST}?parseTime=true"

echo $CN_STRING
echo "---"
# reset test database
echo "--- Reset database"
$GOPATH/bin/goose -dir migrations_test mysql "${CN_STRING}" reset

sleep 1

# run migration && start test
echo "--- New migration database"
$GOPATH/bin/goose -dir migrations_test mysql "${CN_STRING}" up

sleep 1

echo "---"
echo "--- Start to test"
echo "---"
go test ./... -count=1