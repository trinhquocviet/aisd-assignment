#!/bin/bash
go get -u

echo  "${MYSQL_DATABASE}"
goose -dir migrations mysql "${MYSQL_USER}:${MYSQL_PASSWORD}@(${MYSQL_HOST}:${MYSQL_PORT})/${MYSQL_DATABASE}?parseTime=true" up

go run main.go
