package internal

import (
	"fmt"
	logrus "github.com/Sirupsen/logrus"
	v "gopkg.in/go-playground/validator.v9"
	msg "bitbucket.org/trinhquocviet/aisd-assignment/server/internal/message"
)

// Validator struct with custom validate
type Validator struct {
	validator *v.Validate
}

// Validate function
func (r *Validator) Validate(i interface{}) error {
	r.validator = v.New()
	err := r.validator.Struct(i)
	if err != nil {
		logrus.Error("Validate Error: ", err)
		return fmt.Errorf(msg.RequestParamsMismatch)
	}
	return err
}
