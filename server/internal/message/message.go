package message

// DefaultError response error
const DefaultError = "an error has occurred. please try again later"

// MissingToken an error when token is missing
const MissingToken = "Unauthorized: missing the token"

// UnauthorizedToken an error when token is mismatch
const UnauthorizedToken = "Unauthorized Token has been detected"

// UnauthorizedAPI an error when API don't allow
const UnauthorizedAPI = "Unauthorized Access: you do not have permission to access this API"

// CanNotCreateConnection an error when can't create connection
const CanNotCreateConnection = "can't create connection, please try again later"

// UserDoesNotExist an error when get empty user
const UserDoesNotExist = "user doesn't exist"

// RequestParamsMismatch an error when request is mismatch
const RequestParamsMismatch = "request params are mismatch, please try again"

// CanNotGetExchangeRate an error when request is mismatch
const CanNotGetExchangeRate = "get exchange rate error, please try again"
