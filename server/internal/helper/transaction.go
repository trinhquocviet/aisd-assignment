package internal

import (
	"math/rand"
	"strings"
	"time"
)

// GenerateTransactionID generate the TX id, is the single string
func GenerateTransactionID(prefix string) string {
	rand.Seed(time.Now().UnixNano())
	chars := []rune("0123456789")
	length := 16 - len(prefix)

	var stringBuider strings.Builder
	for i := 0; i < length; i++ {
		stringBuider.WriteRune(chars[rand.Intn(len(chars))])
	}
	str := stringBuider.String()

	return prefix + str
}
