
# Introduction
Requires to install before start:

- [docker-machine](https://docs.docker.com/docker-for-mac/install/)
- [docker-compose](https://docs.docker.com/compose/install/)
- [Kitematic - the UI management for docker](https://kitematic.com/)

# Overview Documents

1. Frontend [aisdtest.herokuapp.com](http://asid-assignment.surge.sh/) - API [aisdtest.herokuapp.com](https://aisdtest.herokuapp.com/)

2. DB Diagram [https://dbdiagram.io/d/5dc9a396edf08a25543dc935](https://dbdiagram.io/d/5dc9a396edf08a25543dc935)

3. PostMan to test API after start server [https://www.getpostman.com/collections/d3c20a955c55ae2b9e85](https://www.getpostman.com/collections/d3c20a955c55ae2b9e85)

# Table of Contents
1. [Files & Directories structure](#files--directories-structure)
    - [Project structure](#project-structure)
2. [Usage](#usage)
    - [Start the project with Docker](#start-the-project-with-docker)
3. [Datasheet](#datasheet)
4. Other documents
    - [The answers to your questions](QnA.md)
    - [Frontend's Documents](/web/README.md)
    - [Backend's Documents](/server/README.md)
---
## Files & Directories structure

Describe for all files and directories structure using in this project.

### Project structure
```
.
├── db
│   ├── Dockerfile
│   ├── README.md
├── server
│   ├── api
│   ├── constants
│   ├── internal           # Internal package, only share to internal project
│   ├── migrations         # Contain SQL scripts
│   ├── migrations_test    # Contain SQL scripts include mock-data for testing
│   ├── model
│   ├── routes
│   ├── scripts
│   │   ├── set-up.sh      # Automation script using for docker
|   |   ├── wait-for-it.sh # Script to wait for db container start
│   ├── Dockerfile
│   ├── go.mod
│   ├── go.sum
│   ├── main.go
│   ├── README.md
├── web
│   ├── Dockerfile
│   ├── README.md
│   ├── package.json
├── .env.sample            # Global config for db and docker-compose
├── docker-compose.yml
├── README.md
└── ...
```

## Usage

**NOTE**: If you want to run by your self without using docker, clone this project and put follow this path `$GOPATH/src/bitbucket.org/trinhquocviet/aisd-assignment`, all codebase should place inside this folder.


### Start the project with Docker

**Required: install Docker and docker-compose**, follow the command below to start the project with docker.

01 - Run the command to build the docker containers
```bash
docker-compose build
```

02 - Start all docker containers and waiting for `120s` to make sure all containers are up.
```bash
docker-compose up -d
```

After all docker containers are up, please check on thoses URLs:
- For frontend [http://localhost:3000/](http://localhost:3000/)
- For Apis [http://localhost:1313/](http://localhost:1313/)

**NOTE:** Incase API alway response error, please restart docker

**TO SHUTDOWN DOCKER**
```bash
docker-compose down
```

## Datasheet

Below is the list of accounts available to use

| Name               | Email            | Password |
| ------------------ | ---------------- | -------- |
| Judy Kaszper       | judy@example.com |  123456  |
| Richard Bilborough | rich@example.com |  123456  |


```bash
# Judy's Token

eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30._g50yMPSHfkWTf-V867Uqqn68lo8zBju9-qNUgun47A
```


```bash
# Richard's Token

eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.GxjYTIKTqnSnjH9w7iMRRM4pUW_SgcwKNxCqEc09z2I
```









